package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.connector.dao.CinemaDao;

import static org.junit.Assert.assertEquals;

public class CinemaHibernateDaoTest extends AbstractCinemaTest {

    @Qualifier("cinemaHibernateDao")
    @Autowired
    private CinemaDao cinemaDao;

    @Test
    public void shouldFindByName() {
        assertEquals("Multiplex Dafi", cinemaDao.findByName("Multiplex Dafi").getName());
    }

}
