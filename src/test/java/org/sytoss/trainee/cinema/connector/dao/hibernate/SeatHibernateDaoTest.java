package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.connector.dao.SeatDao;

import static org.junit.Assert.assertEquals;

public class SeatHibernateDaoTest extends AbstractCinemaTest {

    @Qualifier("seatHibernateDao")
    @Autowired
    private SeatDao dao;

    @Test
    public void shouldFindByHall() {
        assertEquals(9, dao.findByHall("Red").size());
    }

    @Test
    public void shouldFindByLine() {
        assertEquals(8, dao.findByLine(1).size());
    }

    @Test
    public void shouldFindByPlace() {
        assertEquals(4, dao.findByPlace(5).size());
    }
}
