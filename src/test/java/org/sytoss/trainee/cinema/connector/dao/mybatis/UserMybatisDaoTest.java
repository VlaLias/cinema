package org.sytoss.trainee.cinema.connector.dao.mybatis;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.connector.dao.UserDao;
import org.sytoss.trainee.cinema.dto.UserDto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class UserMybatisDaoTest extends AbstractCinemaTest {

    @Qualifier("userMybatisDao")
    @Autowired
    private UserDao userDao;

    @Test
    public void shouldFindByLogin() {
        assertNotNull(userDao.findByLogin("sytoss"));
        assertEquals(1, userDao.findByLogin("sytoss").getId());
    }

    @Test
    public void shouldFindById() {
        assertNotNull(userDao.findById(1));
        assertEquals("sytoss1", userDao.findById(1).getLogin());
    }

    @Test
    public void shouldInsert() {
        UserDto user = new UserDto();
        user.setLogin("sytoss3");
        user.setPassword("admin");
        user.setDiscount(15);
        userDao.insert(user);
        assertEquals(user.getPassword(), userDao.findByLogin("sytoss3").getPassword());
    }

    @Test
    public void shouldUpdate() {
        UserDto user = userDao.findById(1);
        user.setLogin("sytoss");
        user.setPassword("12345");
        userDao.update(user);
        assertEquals("12345", userDao.findByLogin("sytoss").getPassword());
    }
}
