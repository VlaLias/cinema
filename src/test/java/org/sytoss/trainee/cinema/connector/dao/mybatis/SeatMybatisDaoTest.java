package org.sytoss.trainee.cinema.connector.dao.mybatis;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.connector.dao.HallDao;
import org.sytoss.trainee.cinema.connector.dao.SeatDao;
import org.sytoss.trainee.cinema.dto.HallDto;
import org.sytoss.trainee.cinema.dto.SeatDto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SeatMybatisDaoTest extends AbstractCinemaTest {

    @Qualifier("seatMybatisDao")
    @Autowired
    private SeatDao seatDao;

    @Qualifier("hallMybatisDao")
    @Autowired
    private HallDao hallDao;

    @Test
    public void shouldInsert() {
        SeatDto seatDTO = new SeatDto();
        seatDTO.setPlace(15);
        seatDTO.setLine(15);
        HallDto hallDTO = hallDao.findById(1);
        seatDTO.setHallDto(hallDTO);
        seatDao.insert(seatDTO);
        assertNotNull(seatDao.findById(19));
    }

    @Test
    public void shouldUpdate() {
        SeatDto seatDTO = seatDao.findById(1);
        seatDTO.setPlace(14);
        seatDTO.setLine(14);
        seatDao.update(seatDTO);
        assertEquals(14, seatDao.findById(1).getPlace());
        assertEquals(14, seatDao.findById(1).getLine());
    }

    @Test
    public void shouldFindById() {
        assertNotNull(seatDao.findById(1));
        assertEquals(1, seatDao.findById(1).getPlace());
        assertEquals(1, seatDao.findById(1).getLine());
    }

    @Test
    public void shouldFindByHall() {
        assertNotNull(seatDao.findByHall("Red"));
        assertEquals(9, seatDao.findByHall("Red").size());
    }

    @Test
    public void shouldFindByLine() {
        assertNotNull(seatDao.findByLine(1));
        assertEquals(8, seatDao.findByLine(1).size());
    }

    @Test
    public void shouldFindByPlace() {
        assertNotNull(seatDao.findByPlace(1));
        assertEquals(3, seatDao.findByPlace(1).size());
    }

}