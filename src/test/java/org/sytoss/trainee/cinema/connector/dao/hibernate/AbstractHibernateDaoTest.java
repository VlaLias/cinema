package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.connector.dao.CinemaDao;
import org.sytoss.trainee.cinema.dto.CinemaDto;

import static org.junit.Assert.assertEquals;

public class AbstractHibernateDaoTest extends AbstractCinemaTest {

    @Autowired
    @Qualifier(value = "cinemaHibernateDao")
    private CinemaDao cinemaDao;

    @Test
    public void shouldInsertCinema() {
        CinemaDto cinemaDTO = new CinemaDto();
        cinemaDTO.setName("My cinema");
        cinemaDTO.setAddress("My street, 1");
        cinemaDao.insert(cinemaDTO);
        assertEquals(cinemaDao.findByName("My cinema").getAddress(), cinemaDTO.getAddress());
    }

    @Test
    public void shouldUpdateCinema() {
        CinemaDto cinemaDTO = cinemaDao.findByName("My cinema");
        cinemaDTO.setAddress("My street, 2");
        cinemaDao.update(cinemaDTO);
        assertEquals(cinemaDao.findByName("My cinema").getAddress(), cinemaDTO.getAddress());
    }

    @Test
    public void shouldFindCinemaById() {
        CinemaDto cinemaDTO = cinemaDao.findById(1);
        assertEquals("Multiplex Dafi", cinemaDTO.getName());
        assertEquals("Heroiv Pratsi Street, 9", cinemaDTO.getAddress());
    }
}
