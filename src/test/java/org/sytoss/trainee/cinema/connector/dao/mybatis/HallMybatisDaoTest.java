package org.sytoss.trainee.cinema.connector.dao.mybatis;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.connector.dao.HallDao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class HallMybatisDaoTest extends AbstractCinemaTest {

    @Autowired
    @Qualifier("hallMybatisDao")
    private HallDao hallDao;

    @Test
    public void shouldFindByName() {
        assertNotNull(hallDao.findByName("Red"));
        assertEquals(1, hallDao.findByName("Red").getId());
    }
}
