package org.sytoss.trainee.cinema.connector.dao.mybatis;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.connector.dao.TicketDao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TicketMybatisDaoTest extends AbstractCinemaTest {

    @Qualifier("ticketMybatisDao")
    @Autowired
    private TicketDao ticketDao;

    @Before
    public void init() {
        super.insertDataset(super.getDataSet("src/main/resources/liquibase/data/ticketDataSet.xml"));
    }

    @Test
    public void shouldFindById() {
        assertNotNull(ticketDao.findById(1));
        double actual = ticketDao.findById(1).getPrice();
        assertEquals(40.0, actual, 0.01);
    }

    @Test
    public void shouldFindBySeance() {
        assertNotNull(ticketDao.findBySeance(1));
        assertEquals(2, ticketDao.findBySeance(1).size());
    }

    @Test
    public void shouldFindByUser() {
        assertNotNull(ticketDao.findByUser("sytoss1"));
        assertEquals(4, ticketDao.findByUser("sytoss1").size());
    }

}