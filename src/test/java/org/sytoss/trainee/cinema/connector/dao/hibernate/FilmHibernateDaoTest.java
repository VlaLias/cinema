package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.connector.dao.FilmDao;

import static org.junit.Assert.assertEquals;

public class FilmHibernateDaoTest extends AbstractCinemaTest {

    @Qualifier("filmHibernateDao")
    @Autowired
    private FilmDao filmDao;

    @Before
    public void init() {
        super.insertDataset(super.getDataSet("src/main/resources/liquibase/data/filmDataSet.xml"));
    }

    @Test
    public void shouldFindByName() {
        assertEquals("Pirates", filmDao.findByName("Pirates").getName());
    }
}
