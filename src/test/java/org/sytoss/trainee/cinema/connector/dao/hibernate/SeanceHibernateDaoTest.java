package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.connector.dao.SeanceDao;

import java.sql.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SeanceHibernateDaoTest extends AbstractCinemaTest {

    @Qualifier("seanceHibernateDao")
    @Autowired
    private SeanceDao seanceDao;

    @Before
    public void init() {
        super.insertDataset(super.getDataSet("src/main/resources/liquibase/data/seanceDataSet.xml"));
    }

    @Test
    public void shouldFindByCinema() {
        assertNotNull(seanceDao.findByCinema("Multiplex Dafi"));
        assertEquals(5, seanceDao.findByCinema("Multiplex Dafi").size());
    }

    @Test
    public void shouldFindByFilm() {
        assertNotNull(seanceDao.findByFilm("Pirates"));
        assertEquals(3, seanceDao.findByFilm("Pirates").size());
    }

    @Test
    public void shouldFindByHall() {
        assertNotNull(seanceDao.findByHall("Red"));
        assertEquals(3, seanceDao.findByHall("Red").size());
    }

    @Test
    public void shouldFindByDate() {
        DateTime date = new DateTime(2019, 8, 25, 0, 0);
        assertNotNull(seanceDao.findByDate(new Date(date.getMillis())));
        assertEquals(3, seanceDao.findByDate(new Date(date.getMillis())).size());
    }
}