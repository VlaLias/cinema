package org.sytoss.trainee.cinema.connector;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.dom.Cinema;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.exception.FileIsEmptyException;
import org.sytoss.trainee.cinema.translator.TicketTranslator;
import org.sytoss.trainee.cinema.util.FileUtil;
import org.sytoss.trainee.cinema.util.TestUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FileSystemConnectorTest extends AbstractCinemaTest {

    private static final String PATH_TO_XML_FILE = "src\\test\\resources\\fileSystemConnectorTestResource.xml";

    @Autowired
    private TicketTranslator ticketTranslator;

    @Test(expected = FileNotFoundException.class)
    public void shouldRaiseFileNotFoundExceptionForSaveCSV() throws IOException {
        List<StringBuilder> ticketTransferObjects = new ArrayList<>();
        FileSystemConnector ticketConnector = new FileSystemConnector();
        ticketConnector.saveCSV(ticketTransferObjects, "C:/all/67.gh");
    }

    @Test
    public void shouldConnect() throws IOException {
        Cinema cinema = TestUtil.createCinema();
        List<StringBuilder> ticketTransferObjects = new ArrayList<>();
        for (Ticket ticket : cinema.getTicketWindows().iterator().next().getTickets()) {
            ticketTransferObjects.add(ticketTranslator.toStringBuilder(ticket));
        }
        FileSystemConnector ticketConnector = new FileSystemConnector();
        ticketConnector.saveCSV(ticketTransferObjects, "src/test/resources/test.csv");

        assertNotNull(ticketConnector);
    }

    @Test(expected = FileNotFoundException.class)
    public void shouldRaiseFileNotFoundExceptionForSaveToXmlWithJdom() throws IOException {
        Document document = new Document();
        FileSystemConnector ticketConnector = new FileSystemConnector();
        ticketConnector.saveToXmlWithJdom(document, "C:/all/67.gh");
    }

    @Test
    public void shouldReadCSV() throws IOException {
        String expectedString = "1,\"Броненосец \"\"Потемкин\"\", часть 2\",\"13.07.2020 21:14\",\"\"Белый\"\",1,1,91.5";
        File file = FileUtil.createTestFile(expectedString, "csv");
        FileSystemConnector connector = new FileSystemConnector();
        List<StringBuilder> result = connector.loadCSV(file);
        Iterator<StringBuilder> resultIterator = result.iterator();
        String actualString = null;
        if (resultIterator.hasNext()) {
            actualString = resultIterator.next().toString();
        }

        assertEquals(expectedString, actualString);
    }

    @Test(expected = FileIsEmptyException.class)
    public void shouldRaiseFileIsEmptyExceptionForReadCSV() throws IOException {
        String expectedString = "";
        File file = FileUtil.createTestFile(expectedString, "csv");
        FileSystemConnector fileSystemConnector = new FileSystemConnector();
        fileSystemConnector.loadCSV(file);
    }

    @Test(expected = IOException.class)
    public void shouldNotSaveToFile() throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        new FileSystemConnector().saveToFile("", outputStream.toByteArray());
    }

    @Test
    public void shouldSaveToFile() throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        new FileSystemConnector().saveToFile(PATH_TO_XML_FILE, outputStream.toByteArray());
    }

    @Test
    public void shouldLoadXMLFile() throws JDOMException, IOException {
        File file = new File(FileSystemConnectorTest.class.getResource("/cinemaTranslatorTestResource.xml").getFile());
        assertEquals("cinema", new FileSystemConnector().loadXMLFile(file.getPath()).getName());
    }
}