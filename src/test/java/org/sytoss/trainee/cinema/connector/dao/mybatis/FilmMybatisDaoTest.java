package org.sytoss.trainee.cinema.connector.dao.mybatis;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.connector.dao.FilmDao;
import org.sytoss.trainee.cinema.dto.FilmDto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FilmMybatisDaoTest extends AbstractCinemaTest {

    @Qualifier("filmMybatisDao")
    @Autowired
    private FilmDao filmMybatisDao;

    @Before
    public void init() {
        super.insertDataset(super.getDataSet("src/main/resources/liquibase/data/filmDataSet.xml"));
    }

    @Test
    public void shouldFindForName() {
        assertNotNull(filmMybatisDao.findByName("Spider Man"));
        assertEquals(2, filmMybatisDao.findByName("Spider Man").getId());
        assertEquals(120, filmMybatisDao.findByName("Spider Man").getDuration());
    }

    @Test
    public void shouldInsertFilm() {
        FilmDto filmDTO = new FilmDto();
        filmDTO.setName("Pirates 2");
        filmDTO.setDuration(125);
        filmMybatisDao.insert(filmDTO);
        assertEquals(filmDTO.getDuration(), filmMybatisDao.findByName("Pirates 2").getDuration());
    }

    @Test
    public void shouldFindById() {
        assertNotNull(filmMybatisDao.findById(1));
        assertEquals("Pirates", filmMybatisDao.findById(1).getName());
    }

    @Test
    public void shouldUpdateFilm() {
        FilmDto filmDTO = filmMybatisDao.findByName("Pirates");
        filmDTO.setDuration(140);
        filmMybatisDao.update(filmDTO);
        assertEquals(140, filmMybatisDao.findByName("Pirates").getDuration());
    }

}
