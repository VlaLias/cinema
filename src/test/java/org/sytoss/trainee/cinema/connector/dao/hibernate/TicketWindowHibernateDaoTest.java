package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.connector.dao.TicketWindowDao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TicketWindowHibernateDaoTest extends AbstractCinemaTest {

    @Qualifier("ticketWindowHibernateDao")
    @Autowired
    private TicketWindowDao ticketWindowDAO;

    @Test
    public void shouldFindByCinema() {
        assertNotNull(ticketWindowDAO.findByCinema("Multiplex Dafi"));
        assertEquals(2, ticketWindowDAO.findByCinema("Multiplex Dafi").size());
    }
}