package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.connector.dao.HallDao;

import static org.junit.Assert.assertEquals;

public class HallHibernateDaoTest extends AbstractCinemaTest {

    @Qualifier("hallHibernateDao")
    @Autowired
    private HallDao hallDao;

    @Test
    public void shouldFindByName() {
        assertEquals("Red", hallDao.findByName("Red").getName());
    }
}
