package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.connector.dao.TicketDao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TicketHibernateDaoTest extends AbstractCinemaTest {

    @Qualifier("ticketHibernateDao")
    @Autowired
    private TicketDao ticketDAO;

    @Before
    public void init() {
        super.insertDataset(super.getDataSet("src/main/resources/liquibase/data/ticketDataSet.xml"));
    }

    @Test
    public void findBySeance() {
        assertNotNull(ticketDAO.findBySeance(1));
        assertEquals(2, ticketDAO.findBySeance(1).size());
    }

    @Test
    public void findByRegisterUser() {
        assertNotNull(ticketDAO.findByUser("sytoss1"));
        assertEquals(4, ticketDAO.findByUser("sytoss1").size());
    }
}