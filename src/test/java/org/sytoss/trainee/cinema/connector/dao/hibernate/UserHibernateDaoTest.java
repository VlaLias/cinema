package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.connector.dao.UserDao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class UserHibernateDaoTest extends AbstractCinemaTest {

    @Qualifier("userHibernateDao")
    @Autowired
    private UserDao userDAO;

    @Test
    public void shouldFindByLogin() {
        assertNotNull(userDAO.findByLogin("sytoss1"));
        assertEquals(1, userDAO.findByLogin("sytoss1").getId());
    }

}