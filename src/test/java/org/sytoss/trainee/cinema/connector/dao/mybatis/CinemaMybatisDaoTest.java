package org.sytoss.trainee.cinema.connector.dao.mybatis;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.connector.dao.CinemaDao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CinemaMybatisDaoTest extends AbstractCinemaTest {

    @Autowired
    @Qualifier(value = "cinemaMybatisDao")
    private CinemaDao cinemaDao;

    @Test
    public void shouldFindByName() {
        assertNotNull(cinemaDao.findByName("Multiplex Dafi"));
        assertEquals(1, cinemaDao.findByName("Multiplex Dafi").getId());
        assertEquals("Heroiv Pratsi Street, 9", cinemaDao.findByName("Multiplex Dafi").getAddress());
    }
}
