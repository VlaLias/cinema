package org.sytoss.trainee.cinema.connector.dao.mybatis;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.connector.dao.TicketWindowDao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TicketWindowMybatisDaoTest extends AbstractCinemaTest {

    @Qualifier("ticketWindowMybatisDao")
    @Autowired
    private TicketWindowDao ticketWindowDao;

    @Test
    public void shouldFindByCinema() {
        assertNotNull(ticketWindowDao.findByCinema("Multiplex Dafi"));
        assertEquals(2, ticketWindowDao.findByCinema("Multiplex Dafi").size());
    }

}