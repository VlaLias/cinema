package org.sytoss.trainee.cinema;

import lombok.extern.slf4j.Slf4j;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.io.FileInputStream;

@Slf4j
@PropertySource({"classpath:db.properties"})
public abstract class AbstractDBTest {

    @Autowired
    private Environment environment;

    protected void insertDataset(IDataSet dataSet) {
        try {
            IDatabaseTester databaseTester = new JdbcDatabaseTester(environment.getProperty("datasource.driverClassName"),
                    environment.getProperty("datasource.url"), environment.getProperty("datasource.username"), environment.getProperty("datasource.password"));
            databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
            databaseTester.setDataSet(dataSet);
            databaseTester.onSetup();
        } catch (Exception e) {
            throw new RuntimeException("Cannot initialiaze DB data", e);
        }
    }

    protected IDataSet getDataSet(String path) {
        IDataSet dataSet = null;
        try {
            dataSet = new FlatXmlDataSetBuilder().build(new FileInputStream(path));
        } catch (Exception e) {
            log.trace(e.getMessage());
        } finally {
            return dataSet;
        }
    }
}
