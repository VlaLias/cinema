package org.sytoss.trainee.cinema.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public final class FileUtil {

    public static File createTestFile(String ticketString, String fileType) throws IOException {
        File file = File.createTempFile("test", fileType);
        FileWriter writer = new FileWriter(file);
        writer.write(ticketString);
        writer.flush();
        writer.close();
        return file;
    }

    public static File createTestFile(List<String> ticketStrings, String fileType) throws IOException {
        File file = File.createTempFile("test", fileType);
        FileWriter writer = new FileWriter(file);
        for (String ticketString : ticketStrings) {
            writer.write(ticketString + System.getProperty("line.separator"));
        }
        writer.flush();
        writer.close();
        return file;
    }
}
