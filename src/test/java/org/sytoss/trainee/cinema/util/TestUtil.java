package org.sytoss.trainee.cinema.util;

import org.joda.time.DateTime;
import org.sytoss.trainee.cinema.dom.Cinema;
import org.sytoss.trainee.cinema.dom.Hall;
import org.sytoss.trainee.cinema.dom.Seance;
import org.sytoss.trainee.cinema.dom.Seat;
import org.sytoss.trainee.cinema.dom.TicketWindow;
import org.sytoss.trainee.cinema.exception.ObjectNotFoundException;

import java.util.Set;
import java.util.TreeSet;

public final class TestUtil {

    private static DateTime seance1StartDate;
    private static DateTime seance2StartDate;
    private static DateTime seance3StartDate;

    private static DateTime scheduleStartDate;
    private static DateTime scheduleEndDate;

    private static Set<Seat> seats = new TreeSet<>();

    static {
        seance1StartDate = new DateTime(2019, 8, 25, 18, 20, 0);
        seance2StartDate = new DateTime(2019, 8, 25, 12, 40, 0);
        seance3StartDate = new DateTime(2019, 8, 26, 18, 20, 0);

        scheduleStartDate = new DateTime(2019, 8, 20, 00, 00, 0);
        scheduleEndDate = new DateTime(2019, 8, 30, 12, 40, 0);

    }

    public static Cinema createCinema() {
        Cinema cinema = new Cinema();
        cinema.setName("Multiplex Dafi");
        cinema.setAddress("Heroiv Pratsi Street, 9");

        TicketWindow ticketWindow = cinema.createTicketWindow(1);
        Hall hall1 = new Hall();
        Hall hall2 = new Hall();
        hall1.setName("Red");
        hall2.setName("Blue");

        Seat seat1 = new Seat();
        seat1.modifyLine(1);
        seat1.modifyPlace(1);

        Seat seat2 = new Seat();
        seat2.modifyLine(1);
        seat2.modifyPlace(2);

        Seat seat3 = new Seat();
        seat3.modifyLine(1);
        seat3.modifyPlace(3);

        Seat seat4 = new Seat();
        seat4.modifyLine(1);
        seat4.modifyPlace(4);

        Seat seat5 = new Seat();
        seat5.modifyLine(1);
        seat5.modifyPlace(5);

        Seat seat6 = new Seat();
        seat6.modifyLine(2);
        seat6.modifyPlace(1);

        Seat seat7 = new Seat();
        seat7.modifyLine(2);
        seat7.modifyPlace(2);

        Seat seat8 = new Seat();
        seat8.modifyLine(2);
        seat8.modifyPlace(3);

        Seat seat9 = new Seat();
        seat9.modifyLine(2);
        seat9.modifyPlace(4);

        Seat seat10 = new Seat();
        seat10.modifyLine(2);
        seat10.modifyPlace(5);

        seats.add(seat1);
        seats.add(seat2);
        seats.add(seat3);
        seats.add(seat4);
        seats.add(seat5);
        seats.add(seat6);
        seats.add(seat7);
        seats.add(seat8);
        seats.add(seat9);
        seats.add(seat10);

        hall1.setSeats(seats);
        hall2.setSeats(seats);

        cinema.addToHalls(hall1);
        cinema.addToHalls(hall2);

        cinema.setSchedule(cinema.createSchedule(scheduleStartDate, scheduleEndDate));

        cinema.createFilm("Pirates", 160);
        cinema.createFilm("Spider Man", 120);
        cinema.createFilm("Броненосец \"Потёмкин\"", 110);

        Seance seance1 = cinema.createSeance(1, "Red", "Pirates", seance1StartDate, 50.0);
        Seance seance2 = cinema.createSeance(2, "Red", "Spider Man", seance2StartDate, 50.0);
        Seance seance3 = cinema.createSeance(3, "Blue", "Броненосец \"Потёмкин\"", seance3StartDate, 50.0);
        Seance seance4 = cinema.createSeance(4, "Blue", "Pirates", seance2StartDate, 50.0);
        Seance seance5 = cinema.createSeance(5, "Red", "Pirates", seance3StartDate, 50.0);

        cinema.createNewUser(1, "sytoss1", 20);
        cinema.createNewUser(2, "sytoss2", 20);

        ticketWindow.addToSeances(seance1);
        ticketWindow.addToSeances(seance2);
        ticketWindow.addToSeances(seance3);
        ticketWindow.addToSeances(seance4);
        ticketWindow.addToSeances(seance5);

        ticketWindow.createTicket(1, seance1, seat2, "sytoss1");
        ticketWindow.createTicket(2, seance1, seat3, "sytoss1");
        ticketWindow.createTicket(3, seance2, seat1, "sytoss2");
        ticketWindow.createTicket(4, seance3, seat1, "sytoss1");
        ticketWindow.createTicket(5, seance3, seat3);
        ticketWindow.createTicket(6, seance4, seat1);
        ticketWindow.createTicket(7, seance5, seat1);


        TicketWindow ticketWindow2 = cinema.createTicketWindow(2);
        ticketWindow2.addToSeances(seance2);
        ticketWindow2.createTicket(8, seance2, seat3, "sytoss2");
        ticketWindow2.createTicket(9, seance2, seat2, "sytoss1");

        return cinema;
    }


    public static Cinema createCinema(String cinemaAddress, String cinemaName) {
        return createCinema(cinemaAddress, cinemaName, "Red", "Blue", "Pirates", 160, "Spider Man", 120,
                1, seance1StartDate, 50.0, 2, seance2StartDate, 50.0,
                1, "sytoss1", 20,
                2, "sytoss2", 20);
    }

    public static Cinema createCinema(String cinemaAddress, String cinemaName,
                                      String hallName1, String hallName2) {
        return createCinema(cinemaAddress, cinemaName, hallName1, hallName2,
                "Pirates", 160, "Spider Man", 120,
                1, seance1StartDate, 50.0, 2, seance2StartDate, 50.0,
                1, "sytoss1", 20,
                2, "sytoss2", 20);
    }

    public static Cinema createCinema(String cinemaAddress, String cinemaName,
                                      String hallName1, String hallName2,
                                      String filmName1, int duration1,
                                      String filmName2, int duration2) {
        return createCinema(cinemaAddress, cinemaName,
                hallName1, hallName2,
                filmName1, duration1, filmName2, duration2,
                1, seance1StartDate, 50.0, 2, seance2StartDate, 50.0,
                1, "sytoss1", 20,
                2, "sytoss2", 20);
    }


    public static Cinema createCinema(String cinemaAddress, String cinemaName,
                                      String hallName1, String hallName2,
                                      String filmName1, int duration1,
                                      String filmName2, int duration2,
                                      int seanceId1, DateTime dateSeance1, double price1,
                                      int seanceId2, DateTime dateSeance2, double price2,
                                      int idUser1, String loginUser1, int discountUser1,
                                      int idUser2, String loginUser2, int discountUser2
    ) throws ObjectNotFoundException {
        if (cinemaAddress == null) {
            throw new IllegalArgumentException("Cinema address cannot be null");
        }
        if (cinemaName == null) {
            throw new IllegalArgumentException("Cinema name cannot be null");
        }
        if (hallName1 == null) {
            throw new IllegalArgumentException("Hall name 1 cannot be null");
        }
        if (hallName2 == null) {
            throw new IllegalArgumentException("Hall name 2 cannot be null");
        }
        if (dateSeance1 == null) {
            throw new IllegalArgumentException("Date seance 1 cannot be null");
        }
        if (dateSeance2 == null) {
            throw new IllegalArgumentException("Date seance 2 cannot be null");
        }

        Cinema cinema = new Cinema();
        cinema.setName(cinemaName);
        cinema.setAddress(cinemaAddress);

        Hall hall1 = new Hall();
        Hall hall2 = new Hall();
        hall1.setName(hallName1);
        hall2.setName(hallName2);
        cinema.addToHalls(hall1);
        cinema.addToHalls(hall2);

        cinema.createFilm(filmName1, duration1);
        cinema.createFilm(filmName2, duration2);

        cinema.createSeance(seanceId1, hallName1, filmName1, dateSeance1, price1);
        cinema.createSeance(seanceId2, hallName1, filmName2, dateSeance2, price2);

        cinema.createNewUser(idUser1, loginUser1, discountUser1);
        cinema.createNewUser(idUser2, loginUser2, discountUser2);

        return cinema;
    }
}
