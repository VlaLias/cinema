package org.sytoss.trainee.cinema.util;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class ReadUtil {

    public static String readUsingScanner(String fileName) throws IOException {
        Path path = Paths.get(fileName);
        Scanner scanner = new Scanner(path);
        String line = "";
        while (scanner.hasNextLine()) {
            line += scanner.nextLine();
            if (scanner.hasNextLine()) {
                line += "\n";
            }
        }
        return line;
    }
}
