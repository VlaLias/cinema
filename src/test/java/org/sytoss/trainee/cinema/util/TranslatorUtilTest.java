package org.sytoss.trainee.cinema.util;

import org.joda.time.DateTime;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.sytoss.trainee.cinema.AbstractCinemaTest;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TranslatorUtilTest extends AbstractCinemaTest {

    @Value("${QUOTE}")
    private String quote;

    @Autowired
    private TranslatorUtil translatorUtil;

    @Test
    public void shouldParsePhrase() {
        String expectedLine = "\"\"test\"\"";
        String actualLine = TranslatorUtil.parsePhrase("\"test\"", quote);
        assertNotNull(actualLine);
        assertEquals(expectedLine, actualLine);
    }

    @Test
    public void shouldCheckForSymbolsWithTrue() {
        assertTrue(translatorUtil.checkForSymbols("test"));
    }

    @Test
    public void shouldCheckForSymbolsWithFalse() {
        assertFalse(translatorUtil.checkForSymbols("test-test"));
    }

    @Test
    public void shouldWrapWithQuotes() {
        StringBuilder stringBuilder = translatorUtil.wrapWithQuotes("test-test", quote);
        assertNotNull(stringBuilder);
        assertEquals("\"test-test\"", stringBuilder.toString());
    }

    @Test
    public void shouldParseDate() {
        DateTime date = new DateTime(2019, 7, 26, 15, 30, 0);
        assertEquals("26.07.2019 15:30", date.toString("dd.MM.yyyy HH:mm"));
    }

}
