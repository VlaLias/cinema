package org.sytoss.trainee.cinema.translator;

import org.jdom2.Attribute;
import org.jdom2.Element;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.dom.Cinema;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.dom.TicketWindow;
import org.sytoss.trainee.cinema.util.FileUtil;
import org.sytoss.trainee.cinema.util.ReadUtil;
import org.sytoss.trainee.cinema.util.TestUtil;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TicketWindowTranslatorTest extends AbstractCinemaTest {

    private static final String PATH_TO_XML_FILE = "src/test/resources/ticketWindowTranslatorTestResource.xml";
    @Autowired
    private TicketWindowTranslator ticketWindowTranslator;

    @Test
    public void shouldTranslateTicketWindow() throws IOException, XmlPullParserException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance(System.getProperty(XmlPullParserFactory.PROPERTY_NAME), null);
        XmlSerializer serializer = factory.newSerializer();
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-indentation", "    ");
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-line-separator", "\n");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        serializer.setOutput(new PrintWriter(outputStream));
        serializer.startDocument("UTF-8", false);
        serializer.text("\n");
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        List<Ticket> tickets = new ArrayList<>(ticketWindow.getTickets());
        ticketWindowTranslator.toSerializer(tickets, serializer);
        serializer.endDocument();
        assertEquals(ReadUtil.readUsingScanner(PATH_TO_XML_FILE),
                new String(outputStream.toByteArray()));
    }


    @Test(expected = IndexOutOfBoundsException.class)
    public void shouldRaiseIndexOutOfBoundsExceptionForToSerializer() throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance(System.getProperty(XmlPullParserFactory.PROPERTY_NAME), null);
        XmlSerializer serializer = factory.newSerializer();
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-indentation", "\t");
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-line-separator", "\n");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        serializer.setOutput(new PrintWriter(outputStream));
        List<Ticket> tickets = new ArrayList<>();
        ticketWindowTranslator.toSerializer(tickets, serializer);
    }

    @Test
    public void shouldConvertTicketWindow() throws XmlPullParserException, IOException {
        File file = FileUtil.createTestFile("<ticketWindow id = \"1\"><ticketWindow>\n", "xml");
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser xpp = factory.newPullParser();
        InputStream input = new FileInputStream(file.getPath());
        xpp.setInput(new InputStreamReader(input));
        xpp.next();
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindowExpected = cinema.getTicketWindows().iterator().next();
        TicketWindow ticketWindowActual = ticketWindowTranslator.toTicketWindow(xpp);
        assertEquals(ticketWindowExpected.getId(), ticketWindowActual.getId());
    }

    @Test
    public void shouldFromElement() {
        Element source = new Element("ticketWindow");
        source.getAttributes().add(new Attribute("id", "1"));
        Element filmElement = new Element("filmElement");
        filmElement.getAttributes().add(new Attribute("name", "Pirates"));
        Element hallElement = new Element("hall");
        hallElement.getAttributes().add(new Attribute("name", "Red"));
        Element seanceElement = new Element("seance");
        seanceElement.getAttributes().add(new Attribute("id", "1"));
        seanceElement.getAttributes().add(new Attribute("startDate", "25.08.2019 18:20"));
        Element ticket = new Element("ticket");
        ticket.getAttributes().add(new Attribute("id", "1"));
        ticket.getAttributes().add(new Attribute("price", "40.0"));
        Element seatElement = new Element("seat");
        seatElement.getAttributes().add(new Attribute("line", "1"));
        seatElement.getAttributes().add(new Attribute("place", "2"));
        Element userElement = new Element("user");
        userElement.getAttributes().add(new Attribute("id", "1"));
        userElement.getAttributes().add(new Attribute("login", "firstsytoss"));
        List<Element> ticketElements = new ArrayList<Element>();
        ticketElements.add(seatElement);
        ticketElements.add(userElement);
        source.setContent(filmElement);
        filmElement.setContent(hallElement);
        hallElement.setContent(seanceElement);
        seanceElement.setContent(ticket);
        ticket.setContent(ticketElements);
        TicketWindow destination = ticketWindowTranslator.fromElement(source);

        assertEquals(1, destination.getId());
        assertEquals("Pirates", destination.getSeances().iterator().next().getFilm().getName());
        assertEquals("Red", destination.getSeances().iterator().next().getHall().getName());
        assertEquals(1, destination.getSeances().iterator().next().getId());
        assertTrue(destination.getSeances().iterator().next().getTickets().iterator().next().equals(destination.getTickets().iterator().next()));
        assertEquals(1, destination.getTickets().iterator().next().getId());
    }
}