package org.sytoss.trainee.cinema.translator;

import org.jdom2.Attribute;
import org.jdom2.Element;
import org.joda.time.DateTime;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.dom.Cinema;
import org.sytoss.trainee.cinema.dom.Seance;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.dom.TicketWindow;
import org.sytoss.trainee.cinema.util.FileUtil;
import org.sytoss.trainee.cinema.util.ReadUtil;
import org.sytoss.trainee.cinema.util.TestUtil;
import org.sytoss.trainee.cinema.util.TranslatorUtil;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SeanceTranslatorTest extends AbstractCinemaTest {

    private static final String PATH_TO_XML_FILE = "src/test/resources/seanceTranslatorTestResource.xml";
    @Autowired
    private SeanceTranslator seanceTranslator;

    @Autowired
    private TranslatorUtil translatorUtil;

    @Test
    public void shouldConvertSeance() throws IOException, XmlPullParserException {
        File file = FileUtil.createTestFile("<seance id = \"1\" startDate = \"25.08.2019 18:20\" ></seance>\n", "xml");
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser xpp = factory.newPullParser();
        InputStream input = new FileInputStream(file.getPath());
        xpp.setInput(new InputStreamReader(input));
        xpp.next();

        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Ticket ticket = ticketWindow.getTickets().iterator().next();

        Seance seance = seanceTranslator.toSeance(xpp);

        assertEquals(seance.getId(), ticket.getSeance().getId());
        assertEquals(seance.getStartDate(), ticket.getSeance().getStartDate());
    }

    @Test
    public void shouldTranslateSeance() throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance(System.getProperty(XmlPullParserFactory.PROPERTY_NAME), null);
        XmlSerializer serializer = factory.newSerializer();
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-indentation", "    ");
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-line-separator", "\n");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        serializer.setOutput(new PrintWriter(outputStream));

        serializer.startDocument("UTF-8", false);
        serializer.text("\n");

        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        List<Ticket> tickets = new ArrayList<>(ticketWindow.getTickets());
        seanceTranslator.toSerializer(tickets, serializer);

        serializer.endDocument();

        assertEquals(ReadUtil.readUsingScanner(PATH_TO_XML_FILE),
                new String(outputStream.toByteArray()));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void shouldRaiseIndexOutOfBoundsExceptionForToSerializer() throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance(System.getProperty(XmlPullParserFactory.PROPERTY_NAME), null);
        XmlSerializer serializer = factory.newSerializer();
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-indentation", "    ");
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-line-separator", "\n");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        serializer.setOutput(new PrintWriter(outputStream));

        List<Ticket> tickets = new ArrayList<>();
        seanceTranslator.toSerializer(tickets, serializer);
    }

    @Test
    public void shouldTranslateFromElement() {

        Element source = new Element("seance");
        source.getAttributes().add(new Attribute("id", "1"));
        source.getAttributes().add(new Attribute("startDate", "25.08.2019 18:20"));

        Element ticket = new Element("ticket");
        ticket.getAttributes().add(new Attribute("id", "1"));
        ticket.getAttributes().add(new Attribute("price", "40.0"));
        Element seatElement = new Element("seat");
        seatElement.getAttributes().add(new Attribute("line", "1"));
        seatElement.getAttributes().add(new Attribute("place", "2"));
        Element userElement = new Element("user");
        userElement.getAttributes().add(new Attribute("id", "1"));
        userElement.getAttributes().add(new Attribute("login", "firstsytoss"));
        List<Element> ticketElements = new ArrayList<>();
        ticketElements.add(seatElement);
        ticketElements.add(userElement);
        source.setContent(ticket);
        ticket.setContent(ticketElements);

        TicketWindow ticketWindow = new TicketWindow();
        ticketWindow.modifyId(1);
        Seance destination = seanceTranslator.fromElement(source, ticketWindow);
        DateTime date = translatorUtil.parseStringToDate("25.08.2019 18:20");

        assertEquals(1, destination.getId());
        assertTrue(date.getMillis() == destination.getStartDate().getMillis());
        assertEquals(1, destination.getTickets().iterator().next().getTicketWindow().getId());
        assertEquals(1, destination.getTickets().iterator().next().getId());
        assertEquals(40.0, destination.getTickets().iterator().next().getPrice(), 0.01);
    }
}