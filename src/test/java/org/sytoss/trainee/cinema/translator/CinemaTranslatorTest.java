package org.sytoss.trainee.cinema.translator;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.dom.Cinema;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.dom.TicketWindow;
import org.sytoss.trainee.cinema.util.ReadUtil;
import org.sytoss.trainee.cinema.util.TestUtil;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;

public class CinemaTranslatorTest extends AbstractCinemaTest {

    private static final String PATH_TO_XML_FILE = "src/test/resources/cinemaTranslatorTestResource.xml";

    @Autowired
    private CinemaTranslator cinemaTranslator;

    @Test(expected = NoSuchElementException.class)
    public void shouldRaiseNoSuchElementExceptionForToSerializer() throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance(System.getProperty(XmlPullParserFactory.PROPERTY_NAME), null);
        XmlSerializer serializer = factory.newSerializer();
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-indentation", "\t");
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-line-separator", "\n");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        serializer.setOutput(new PrintWriter(outputStream));
        Set<Ticket> tickets = new TreeSet<>();
        cinemaTranslator.toSerializer(tickets, serializer);
    }

    @Test
    public void shouldTranslateFilm() throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance(System.getProperty(XmlPullParserFactory.PROPERTY_NAME), null);
        XmlSerializer serializer = factory.newSerializer();
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-indentation", "    ");
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-line-separator", "\n");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        serializer.setOutput(new PrintWriter(outputStream));


        serializer.startDocument("UTF-8", false);
        serializer.text("\n");

        Cinema cinema = TestUtil.createCinema();
        Set<Ticket> tickets = new TreeSet<>();
        for (TicketWindow ticketWindow : cinema.getTicketWindows()) {
            for (Ticket ticket : ticketWindow.getTickets()) {
                tickets.add(ticket);
            }
        }
        cinemaTranslator.toSerializer(tickets, serializer);

        serializer.endDocument();
        assertEquals(ReadUtil.readUsingScanner(PATH_TO_XML_FILE),
                new String(outputStream.toByteArray()));
    }
}
