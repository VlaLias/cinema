package org.sytoss.trainee.cinema.translator;

import org.jdom2.Attribute;
import org.jdom2.Element;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.dom.Cinema;
import org.sytoss.trainee.cinema.dom.Hall;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.dom.TicketWindow;
import org.sytoss.trainee.cinema.util.FileUtil;
import org.sytoss.trainee.cinema.util.ReadUtil;
import org.sytoss.trainee.cinema.util.TestUtil;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class HallTranslatorTest extends AbstractCinemaTest {

    private static final String PATH_TO_XML_FILE = "src/test/resources/hallTranslatorTestResource.xml";
    @Autowired
    private HallTranslator hallTranslator;

    @Test
    public void shouldTranslateFromElement() {
        Element source = new Element("hall");
        source.getAttributes().add(new Attribute("name", "Red"));
        Hall destination = hallTranslator.fromElement(source);

        assertEquals("Red", destination.getName());
    }

    @Test
    public void shouldConvertHall() throws XmlPullParserException, IOException {
        File file = FileUtil.createTestFile("<hall name = \"Red\"></hall>\n", "xml");
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser xpp = factory.newPullParser();
        InputStream input = new FileInputStream(file.getPath());
        xpp.setInput(new InputStreamReader(input));
        xpp.next();

        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Ticket ticket = ticketWindow.getTickets().iterator().next();

        Hall hall = hallTranslator.convertHall(xpp);

        assertEquals(hall.getName(), ticket.getSeance().getHall().getName());

    }

    @Test
    public void shouldTranslateHall() throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance(System.getProperty(XmlPullParserFactory.PROPERTY_NAME), null);
        XmlSerializer serializer = factory.newSerializer();
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-indentation", "    ");
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-line-separator", "\n");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        serializer.setOutput(new PrintWriter(outputStream));

        serializer.startDocument("UTF-8", false);
        serializer.text("\n");

        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        List<Ticket> tickets = new ArrayList<>(ticketWindow.getTickets());
        hallTranslator.toSerializer(tickets, serializer);

        serializer.endDocument();

        assertEquals(ReadUtil.readUsingScanner(PATH_TO_XML_FILE),
                new String(outputStream.toByteArray()));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void shouldRaiseIndexOutOfBoundsExceptionForToSerializer() throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance(System.getProperty(XmlPullParserFactory.PROPERTY_NAME), null);
        XmlSerializer serializer = factory.newSerializer();
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-indentation", "\t");
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-line-separator", "\n");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        serializer.setOutput(new PrintWriter(outputStream));

        List<Ticket> tickets = new ArrayList<>();
        hallTranslator.toSerializer(tickets, serializer);
    }
}