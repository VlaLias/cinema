package org.sytoss.trainee.cinema.translator;

import org.jdom2.Attribute;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.dom.Cinema;
import org.sytoss.trainee.cinema.dom.Seat;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.dom.TicketWindow;
import org.sytoss.trainee.cinema.util.FileUtil;
import org.sytoss.trainee.cinema.util.ReadUtil;
import org.sytoss.trainee.cinema.util.TestUtil;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import static org.junit.Assert.assertEquals;

public class SeatTranslatorTest extends AbstractCinemaTest {

    private static final String PATH_TO_XML_FILE = "src/test/resources/seatTranslatorTestResource.xml";
    @Autowired
    private SeatTranslator seatTranslator;

    @Test
    public void shouldTranslateSeat() throws IOException, XmlPullParserException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance(System.getProperty(XmlPullParserFactory.PROPERTY_NAME), null);
        XmlSerializer serializer = factory.newSerializer();
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-indentation", "\t");
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-line-separator", "\n");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        serializer.setOutput(new PrintWriter(outputStream));

        serializer.startDocument("UTF-8", false);
        serializer.text("\n");
        Ticket ticket = new Ticket();
        Seat seat = new Seat();
        seat.modifyLine(13);
        seat.modifyPlace(5);
        ticket.setSeat(seat);
        seatTranslator.toSerializer(ticket, serializer);
        serializer.endDocument();

        assertEquals(ReadUtil.readUsingScanner(PATH_TO_XML_FILE),
                new String(outputStream.toByteArray()));
    }

    @Test
    public void shouldParseXML() throws XmlPullParserException, IOException {

        File file = FileUtil.createTestFile("<seat line = \"1\" place = \"2\"></seat>\n", "xml");
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser xpp = factory.newPullParser();
        InputStream input = new FileInputStream(file.getPath());
        xpp.setInput(new InputStreamReader(input));
        xpp.next();

        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Ticket ticket = ticketWindow.getTickets().iterator().next();

        Seat seat = seatTranslator.toSeat(xpp);

        assertEquals(seat.getLine(), ticket.getSeat().getLine());
        assertEquals(seat.getPlace(), ticket.getSeat().getPlace());
    }

    @Test
    public void shouldTranslateFromElement() throws JDOMException, IOException {
        Element source = new Element("seat");
        source.getAttributes().add(new Attribute("line", "1"));
        source.getAttributes().add(new Attribute("place", "2"));
        Seat destination = seatTranslator.fromElement(source);

        assertEquals(1, destination.getLine());
        assertEquals(2, destination.getPlace());
    }
}