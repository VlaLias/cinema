package org.sytoss.trainee.cinema.translator;

import org.jdom2.Attribute;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.joda.time.DateTime;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.dom.Cinema;
import org.sytoss.trainee.cinema.dom.Seance;
import org.sytoss.trainee.cinema.dom.Seat;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.dom.TicketWindow;
import org.sytoss.trainee.cinema.util.ReadUtil;
import org.sytoss.trainee.cinema.util.TestUtil;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TicketTranslatorTest extends AbstractCinemaTest {

    private static final String PATH_TO_XML_FILE = "src/test/resources/ticketTranslatorTestResource.xml";
    @Autowired
    private TicketTranslator ticketTranslator;

    @Test
    public void shouldParseLine() {
        StringBuilder testString = new StringBuilder("1,\"Броненосец \"\"Потемкин\"\", часть 2\",\"13.07.2018 21:14\",\"Белый\",1,1,91.5");
        Ticket actualTicket = ticketTranslator.toTicket(testString);

        assertEquals(1, actualTicket.getId());
        assertEquals("Броненосец \"Потемкин\", часть 2", actualTicket.getSeance().getFilm().getName());
        assertEquals("13.07.2018 21:14", actualTicket.getSeance().getStartDate().toString("dd.MM.yyyy HH:mm"));
        assertEquals("Белый", actualTicket.getSeance().getHall().getName());
        assertEquals(1, actualTicket.getSeat().getLine());
        assertEquals(1, actualTicket.getSeat().getPlace());
        assertEquals(91.5, actualTicket.getPrice(), 0.001);
    }

    @Test(expected = NumberFormatException.class)
    public void shouldRaiseNumberFormatExceptionForParseLine() throws NumberFormatException {
        StringBuilder testString = new StringBuilder("s,\"Броненосец \"\"Потемкин\"\", часть 2\",\"13.07.2018 21:14\",\"\"Белый\"\",1,1,91.5");
        ticketTranslator.toTicket(testString);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseIllegalArgumentExceptionForParseLine() throws NumberFormatException {
        StringBuilder testString = new StringBuilder("1,\"Броненосец \"\"Потемкин\"\", часть 2\",\"13-07-2018 21:14\",\"\"Белый\"\",1,1,91.5");
        ticketTranslator.toTicket(testString);
    }

    @Test
    public void shouldConvertToTicketTransferObject() {
        DateTime date = new DateTime(2019, 8, 29, 00, 00, 00);

        Cinema cinema = TestUtil.createCinema();
        cinema.createFilm("Titanic", 180);
        Seance seance = cinema.createSeance(1, "Red", "Titanic", date, 200.0);
        Seat seat = new Seat();
        seat.modifyLine(5);
        seat.modifyPlace(6);
        Set<Seat> seatSet = new TreeSet<>();
        seatSet.add(seat);
        seance.getHall().setSeats(seatSet);
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Ticket ticket = ticketWindow.createTicket(5, seance, seat);

        assertNotNull(ticketTranslator.toStringBuilder(ticket));
        assertEquals("5,Titanic,\"29.08.2019 00:00\",Red,5,6,200.0", ticketTranslator.toStringBuilder(ticket).toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseIllegalArgumentExceptionForConvertToTicketTransferObject() {

        DateTime date = new DateTime(2017, 7, 21, 00, 00, 00);

        Cinema cinema = TestUtil.createCinema();
        cinema.createFilm("Titanic", 180);
        Seance seance = cinema.createSeance(1, "Red", "Titanic", date, 200.0);
        Seat seat = new Seat();
        seat.modifyLine(5);
        seat.modifyPlace(6);
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Ticket ticket = ticketWindow.createTicket(5, seance, seat);

        assertNotNull(ticketTranslator.toStringBuilder(ticket));
    }

    @Test
    public void shouldTranslateTicket() throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance(System.getProperty(XmlPullParserFactory.PROPERTY_NAME), null);
        XmlSerializer serializer = factory.newSerializer();
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-indentation", "    ");
        serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-line-separator", "\n");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        serializer.setOutput(new PrintWriter(outputStream));

        serializer.startDocument("UTF-8", false);
        serializer.text("\n");

        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Ticket ticket = ticketWindow.getTickets().iterator().next();
        ticketTranslator.toSerializer(ticket, serializer);

        serializer.endDocument();

        assertEquals(ReadUtil.readUsingScanner(PATH_TO_XML_FILE),
                new String(outputStream.toByteArray()));
    }

    @Test
    public void shouldConvertToTickets() throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser xpp = factory.newPullParser();
        InputStream input = new FileInputStream("src/test/resources/ticketServiceTestResource.xml");
        xpp.setInput(new InputStreamReader(input));

        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Ticket ticket = ticketWindow.getTickets().iterator().next();

        Set<Ticket> tickets = ticketTranslator.fromPullParser(xpp);
        assertEquals(ticket, tickets.iterator().next());
    }

    @Test
    public void shouldTranslateFromElement() throws JDOMException {
        Element source = new Element("ticket");
        source.getAttributes().add(new Attribute("id", "1"));
        source.getAttributes().add(new Attribute("price", "40.0"));
        Element seatElement = new Element("seat");
        seatElement.getAttributes().add(new Attribute("line", "1"));
        seatElement.getAttributes().add(new Attribute("place", "2"));
        Element userElement = new Element("user");
        userElement.getAttributes().add(new Attribute("id", "1"));
        userElement.getAttributes().add(new Attribute("login", "firstsytoss"));
        List<Element> sourceElements = new ArrayList<Element>();
        sourceElements.add(seatElement);
        sourceElements.add(userElement);
        source.setContent(sourceElements);
        Ticket destination = ticketTranslator.fromElement(source);

        assertEquals(1, destination.getId());
        assertEquals(40.0, destination.getPrice(), 0.01);
        assertEquals(1, destination.getSeat().getLine());
        assertEquals(2, destination.getSeat().getPlace());
        assertEquals("firstsytoss", destination.getUser().getLogin());
    }
}