package org.sytoss.trainee.cinema.dom;

import org.joda.time.DateTime;
import org.junit.Test;
import org.sytoss.trainee.cinema.exception.ObjectNotFoundException;
import org.sytoss.trainee.cinema.util.TestUtil;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class CinemaTest {
    @Test
    public void shouldCreateCinema() {
        Cinema cinema = new Cinema();
        cinema.setName("Multiplex Dafi");
        cinema.setAddress("Heroiv Pratsi Street, 9");
        assertNotNull(cinema);
        assertEquals("Heroiv Pratsi Street, 9", cinema.getAddress());
        assertEquals("Multiplex Dafi", cinema.getName());
    }

    @Test
    public void shouldFindRegUserByLogin() {
        Cinema cinema = TestUtil.createCinema();
        RegisterUser registerUser = new RegisterUser();
        registerUser.setLogin("sytoss");
        cinema.addToRegisterUsers(registerUser);
        cinema.findRegisterUser("sytoss");
        assertNotNull(registerUser);
        assertNotNull(cinema.findRegisterUser("sytoss"));
        assertEquals(registerUser, cinema.findRegisterUser("sytoss"));
    }

    @Test(expected = ObjectNotFoundException.class)
    public void shouldRaiseObjectNotFoundExceptionForFindRegisterUser() {
        Cinema cinema = TestUtil.createCinema();
        cinema.findRegisterUser("sytoss");
    }

    @Test(expected = NullPointerException.class)
    public void shouldRaiseNullPointerExceptionForGetRegisterUsers() {
        Cinema cinema = TestUtil.createCinema();
        Set<RegisterUser> registerUsers = null;
        cinema.setRegisterUsers(registerUsers);
        cinema.getRegisterUsers();
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseIllegalArgumentExceptionForAddToRegisterUsers() {
        Cinema cinema = TestUtil.createCinema();
        RegisterUser registerUser = null;
        cinema.addToRegisterUsers(registerUser);
    }

    @Test
    public void shouldFindFilmByName() {
        Cinema cinema = TestUtil.createCinema();
        cinema.createFilm("Pirates", 160);
        Film film = cinema.findFilmByName("Pirates");
        assertNotNull(film);
        assertEquals("Pirates", film.getName());
        assertEquals(160, film.getDuration());

    }

    @Test
    public void shouldCreateFilm() {
        Cinema cinema = TestUtil.createCinema();
        cinema.createFilm("Pirates", 160);

        assertNotNull(cinema.findFilmByName("Pirates"));
        assertEquals("Pirates", cinema.findFilmByName("Pirates").getName());
        assertEquals(160, cinema.findFilmByName("Pirates").getDuration());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseIllegalArgumentExceptionForCreateFilmWithNameNull() {
        Cinema cinema = TestUtil.createCinema();
        cinema.createFilm(null, 160);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseIllegalArgumentExceptionForCreateFilmWithDurationNegative() {
        Cinema cinema = TestUtil.createCinema();
        cinema.createFilm("Pirates", -1);
    }

    @Test
    public void shouldFindHallByName() {
        Cinema cinema = TestUtil.createCinema();
        Hall hall = new Hall();
        hall.setName("Yellow");
        cinema.addToHalls(hall);
        Hall tempHall = cinema.findHallByName("Yellow");
        assertNotNull(hall);
        assertEquals("Yellow", tempHall.getName());
    }

    @Test(expected = ObjectNotFoundException.class)
    public void shouldRaiseObjectNotFoundExceptionForFindHallByName() {
        Cinema cinema = TestUtil.createCinema();
        Hall hall = new Hall();
        hall.setName("Red1");
        cinema.addToHalls(hall);
        cinema.findHallByName("Red2");
    }

    @Test
    public void shouldCreateSeance() {
        Cinema cinema = TestUtil.createCinema();
        DateTime dateTime = new DateTime(2019, 7, 26, 15, 30, 0);
        Seance seance = cinema.createSeance(cinema.getSeances().size() + 1, "Red", "Pirates", dateTime, 50.0);
        assertNotNull(seance);
        assertEquals("Red", seance.getHall().getName());
        assertEquals(dateTime, seance.getStartDate());
        assertEquals("Pirates", seance.getFilm().getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseIllegalArgumentExceptionForCreateSeanceWithSameDate() {
        Cinema cinema = TestUtil.createCinema();
        DateTime dateTime = new DateTime(2021, 8, 25, 18, 20, 0);
        cinema.createSeance(cinema.getSeances().size() + 1, "Red", "Pirates", dateTime, 50.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseIllegalArgumentExceptionForCreateSeanceWithNullHall() {
        Cinema cinema = TestUtil.createCinema();
        DateTime dateTime = new DateTime(2017, 7, 25, 18, 20, 0);
        cinema.createSeance(cinema.getSeances().size() + 1, null, "Pirates", dateTime, 50.0);
    }


    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseIllegalArgumentExceptionForCreateSeanceWithNullNameFilm() {
        Cinema cinema = TestUtil.createCinema();
        DateTime dateTime = new DateTime(2017, 7, 25, 18, 20, 0);
        cinema.createSeance(cinema.getSeances().size() + 1, "Red", null, dateTime, 50.0);
    }

    @Test(expected = ObjectNotFoundException.class)
    public void shouldRaiseObjectNotFoundExceptionForCreateSeance() {
        Cinema cinema = TestUtil.createCinema();
        DateTime dateTime = new DateTime(2020, 8, 25, 18, 20, 0);
        cinema.createSeance(cinema.getSeances().size() + 1, "Red", "Pirates3", dateTime, 50.0);
    }

    @Test
    public void shouldCreateSchedule() {
        DateTime scheduleStartDate = new DateTime(2020, 8, 31, 10, 00, 0);
        DateTime scheduleEndDate = new DateTime(2020, 9, 6, 00, 00, 0);
        Cinema cinema = TestUtil.createCinema();
        assertNotNull(cinema.createSchedule(scheduleStartDate, scheduleEndDate));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseIllegalArgumentExceptionCreateSchedule() {
        DateTime scheduleStartDate = new DateTime(2017, 7, 26, 10, 00, 0);
        DateTime scheduleEndDate = new DateTime(2017, 8, 6, 00, 00, 0);

        Cinema cinema = TestUtil.createCinema();
        cinema.createSchedule(scheduleStartDate, scheduleEndDate);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseIllegalArgumentExceptionWithStartDateBeforeEnd() {
        DateTime scheduleStartDate = new DateTime(2017, 7, 15, 10, 00, 0);
        DateTime scheduleEndDate = new DateTime(2017, 8, 6, 00, 00, 0);

        Cinema cinema = TestUtil.createCinema();
        cinema.createSchedule(scheduleStartDate, scheduleEndDate);
    }

    @Test
    public void shouldAddToTicketWindows() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        cinema.addToTicketWindows(ticketWindow);

        assertTrue(cinema.getTicketWindows().contains(ticketWindow));
        assertEquals(ticketWindow.getCinema(), cinema);
    }
}
