package org.sytoss.trainee.cinema.dom;

import org.joda.time.DateTime;
import org.junit.Test;
import org.sytoss.trainee.cinema.util.TestUtil;

import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class SeanceTest {

    @Test
    public void shouldGetTickets() {
        Seance seance = new Seance();
        Set<Ticket> tickets = seance.getTickets();
        assertNotNull(tickets);
    }

    @Test
    public void shouldCheckSeat() {
        Cinema cinema = TestUtil.createCinema();
        Seance seance = cinema.getSeances().iterator().next();
        Seat seatToReserve = new Seat();
        seatToReserve.modifyLine(1);
        seatToReserve.modifyPlace(1);
        assertTrue(seance.checkSeat(seatToReserve));
    }

    @Test
    public void shouldNotCheckSeat() {
        Cinema cinema = TestUtil.createCinema();
        Seance seance = cinema.getSeances().iterator().next();
        Seat seatToReserve = new Seat();
        seatToReserve.modifyLine(1);
        seatToReserve.modifyPlace(2);
        assertFalse(seance.checkSeat(seatToReserve));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseIllegalArgumentExceptionForAddToTickets() {
        Seance seance = new Seance();
        Ticket ticket = null;
        seance.addToTickets(ticket);
    }

    @Test
    public void shouldRemoveTicket() {
        Seance seance = new Seance();
        Seat seat = new Seat();
        Ticket ticket = new Ticket();
        ticket.modifyId(1);
        ticket.modifyPrice(99.0);
        ticket.setSeat(seat);
        ticket.modifySeance(seance);
        seance.addToTickets(ticket);
        seance.deleteTicket(ticket);
        assertFalse(seance.getTickets().contains(ticket));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseIllegalArgumentExceptionForModifyPrice() {
        Seance seance = new Seance();
        DateTime date = new DateTime(2016, 12, 12, 00, 00, 0);
        seance.modifyStartDate(date);
    }

    @Test
    public void shouldAddToTickets() {
        Cinema cinema = TestUtil.createCinema();
        Iterator<Ticket> iterator = cinema.getTicketWindows().iterator().next().getTickets().iterator();
        Seance seance = iterator.next().getSeance();
        iterator.next();
        Ticket ticket = iterator.next();

        assertFalse(seance.getTickets().contains(ticket));
        seance.addToTickets(ticket);
        assertTrue(seance.getTickets().contains(ticket));
    }

}
