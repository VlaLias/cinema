package org.sytoss.trainee.cinema.dom;

import org.junit.Test;
import org.sytoss.trainee.cinema.exception.ObjectNotFoundException;
import org.sytoss.trainee.cinema.util.TestUtil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TicketWindowTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseExceptionWhenIdTicketWindowIsNull() {
        Cinema cinema = TestUtil.createCinema();
        cinema.getTicketWindows().iterator().next().modifyId(0);
    }

    @Test
    public void shouldCalculateIncomeBySeance() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Seance seance = cinema.findSeanceById(1);
        assertEquals("Incorrect calculation", (Object) 80.0, ticketWindow.calculateIncomeBySeance(seance));
    }

    @Test
    public void shouldCalculateTotalIncome() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        assertEquals("Incorrect calculation", (Object) 310.0, ticketWindow.calculateTotalIncome());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseExceptionWhenTryToAddExistedTicket() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Seance seance = cinema.getSeances().iterator().next();
        seance.modifyId(1);
        Seat seat = seance.getHall().getSeats().iterator().next();
        seat.modifyPlace(3);
        Ticket ticket = ticketWindow.createTicket(1, seance, seat);
        ticketWindow.addToTickets(ticket);

    }

    @Test
    public void shouldCheckSeanceExistence() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Seance seance = cinema.findSeanceById(1);
        assertTrue(ticketWindow.isExistedSeance(seance));
    }

    @Test
    public void shouldNotCheckSeanceExistence() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        assertFalse(ticketWindow.isExistedSeance(new Seance()));
    }

    @Test
    public void shouldFindSeanceById() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Seance testSeance = ticketWindow.findSeanceById(1);
        assertEquals((Object) 1, testSeance.getId());
    }

    @Test(expected = ObjectNotFoundException.class)
    public void shouldRaiseExceptionWhenTryToFindSeanceByUnexistedId() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        ticketWindow.findSeanceById(7);
    }

    @Test
    public void shouldCheckTicketExistence() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Ticket ticket = ticketWindow.getTickets().iterator().next();
        assertTrue(ticketWindow.checkTicketExistence(ticket));
    }

    @Test
    public void shouldCheckTicketUsability() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Ticket ticket = ticketWindow.getTickets().iterator().next();
        assertTrue(ticketWindow.checkTicketUsability(ticket));
    }

    @Test
    public void shouldBuyTicketWithDiscount() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        assertEquals(40.0, ticketWindow.getTickets().iterator().next().getPrice(), 0.0);
    }

    @Test
    public void shouldDeleteTicket() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Ticket ticket = ticketWindow.getTickets().iterator().next();
        ticketWindow.deleteTicket(ticket);
        assertFalse(ticketWindow.getTickets().contains(ticket));
        assertEquals(null, ticket.getTicketWindow());
        assertEquals(null, ticket.getUser());
        assertEquals(null, ticket.getSeance());
    }

    @Test
    public void shouldCreateTicketForRegisterUser() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Seance seance = cinema.getSeances().iterator().next();
        Seat seat = new Seat();
        seat.modifyLine(1);
        seat.modifyPlace(5);
        RegisterUser registerUser = cinema.getRegisterUsers().iterator().next();
        String login = registerUser.getLogin();
        assertEquals(seat.getLine(), ticketWindow.createTicket(804, seance, seat, login).getSeat().getLine());
    }

    @Test(expected = ObjectNotFoundException.class)
    public void shouldNotCreateTicketForRegisterUser() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Seance seance = cinema.getSeances().iterator().next();
        Seat seat = new Seat();
        seat.modifyLine(1);
        seat.modifyPlace(11);
        RegisterUser registerUser = cinema.getRegisterUsers().iterator().next();
        ticketWindow.createTicket(804, seance, seat, registerUser.getLogin());
    }

    @Test
    public void shouldCreateTicketForUnregisterUser() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Seance seance = cinema.getSeances().iterator().next();
        Seat seat = new Seat();
        seat.modifyLine(1);
        seat.modifyPlace(4);
        assertEquals(seat.getLine(), ticketWindow.createTicket(804, seance, seat).getSeat().getLine());
    }

    @Test(expected = ObjectNotFoundException.class)
    public void shouldNotCreateTicketForUnregisterUser() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Seance seance = cinema.getSeances().iterator().next();
        Seat seat = new Seat();
        seat.modifyLine(100);
        seat.modifyPlace(1);
        ticketWindow.createTicket(804, seance, seat);
    }

    @Test
    public void shouldModifyCinema() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        ticketWindow.modifyCinema(cinema);

        assertEquals(ticketWindow.getCinema(), cinema);
        assertTrue(cinema.getTicketWindows().contains(ticketWindow));
    }

    @Test
    public void shouldAddTicketWithoutTicketWindow() {
        Cinema cinema = TestUtil.createCinema();
        Ticket ticket = new Ticket();
        ticket.modifyId(10);
        ticket.modifyPrice(50.0);
        ticket.modifySeance(cinema.getSeances().iterator().next());
        ticket.modifyRegisterUser(cinema.getRegisterUsers().iterator().next());
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        ticket.setSeat(cinema.getHalls().iterator().next().getSeats().iterator().next());
        ticketWindow.addToTickets(ticket);
        assertEquals(ticket.getTicketWindow(), ticketWindow);
        assertTrue(ticketWindow.getTickets().contains(ticket));
    }
}