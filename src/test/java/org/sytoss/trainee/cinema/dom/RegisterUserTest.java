package org.sytoss.trainee.cinema.dom;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class RegisterUserTest {

    @Test
    public void shouldSetDiscount() {
        RegisterUser registerUser = new RegisterUser();
        registerUser.modifyDiscount(10);
        Assert.assertEquals(10, registerUser.getDiscount());
    }

    @Test
    public void shouldSetLogin() {
        RegisterUser registerUser = new RegisterUser();
        registerUser.setLogin("Stich");
        Assert.assertEquals("Stich", registerUser.getLogin());
    }

    @Test
    public void shouldCheckLoginIsNull() {
        RegisterUser registerUser = new RegisterUser();
        registerUser.setLogin(null);
        assertNull(registerUser.getLogin());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseErrorWhenDiscountIsNegative() {
        RegisterUser registerUser = new RegisterUser();
        registerUser.modifyDiscount(-10);
    }

    @Test
    public void shouldSetBoughtTickets() {
        RegisterUser registerUser = new RegisterUser();
        Ticket ticket = new Ticket();
        registerUser.addToTickets(ticket);
        Assert.assertTrue(registerUser.getTickets().contains(ticket));
    }

    @Test
    public void shouldAddTicketToUser() {
        Ticket ticket = new Ticket();
        RegisterUser registerUser = new RegisterUser();
        registerUser.addToTickets(ticket);
        assertNotNull(ticket.getUser());
        assertEquals(registerUser, ticket.getUser());
    }

    @Test
    public void shouldSwapTicketToAnother() {
        Ticket ticket = new Ticket();
        RegisterUser user = new RegisterUser();
        user.setLogin("old");
        assertEquals(0, user.getTickets().size());
        user.addToTickets(ticket);
        assertEquals(1, user.getTickets().size());
        RegisterUser newUser = new RegisterUser();
        newUser.setLogin("new");
        newUser.addToTickets(ticket);
        assertEquals(1, user.getTickets().size());
        assertEquals(1, newUser.getTickets().size());
    }

}
