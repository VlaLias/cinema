package org.sytoss.trainee.cinema.dom;

import org.junit.Test;
import org.sytoss.trainee.cinema.util.TestUtil;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class TicketTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseIllegalArgumentExceptionForModifyPrice() {
        Ticket ticket = new Ticket();
        ticket.modifyPrice(0);
    }

    @Test
    public void shouldCheckSeatIsNull() {
        Ticket ticket = new Ticket();
        Seat seat = null;
        ticket.setSeat(seat);
        assertNull(ticket.getSeat());
    }

    @Test
    public void shouldModifyTicketWindow() {
        Cinema cinema = TestUtil.createCinema();
        TicketWindow ticketWindowToChange = new TicketWindow();
        TicketWindow ticketWindow = cinema.getTicketWindows().iterator().next();
        Ticket ticket = ticketWindow.getTickets().iterator().next();

        for (Seance seance : ticketWindow.getSeances()) {
            ticketWindowToChange.addToSeances(seance);
        }
        ticket.modifyTicketWindow(ticketWindowToChange);
        assertEquals(ticket.getTicketWindow(), ticketWindowToChange);
        assertTrue(ticketWindowToChange.getTickets().contains(ticket));
        assertFalse(ticketWindow.getTickets().contains(ticket));
    }

    @Test
    public void shouldModifySeance() {
        Ticket ticket = new Ticket();
        Seance seance = new Seance();

        assertTrue(seance.getTickets().isEmpty());
        ticket.modifySeance(seance);
        assertFalse(seance.getTickets().isEmpty());
    }

    @Test
    public void shouldModifySeanceToAnother() {
        Cinema cinema = TestUtil.createCinema();
        Ticket ticket = cinema.getTicketWindows().iterator().next().getTickets().iterator().next();
        Iterator<Seance> iterator = cinema.getSeances().iterator();
        Seance seance = ticket.getSeance();
        iterator.next();
        Seance newSeance = iterator.next();
        ticket.modifySeance(newSeance);

        assertTrue(newSeance.getTickets().contains(ticket));
        assertFalse(seance.getTickets().isEmpty());
    }

    @Test
    public void shouldAddUserToTicket() {
        Ticket ticket = new Ticket();
        ticket.modifyId(1);
        RegisterUser registerUser = new RegisterUser();
        assertNull(ticket.getUser());
        ticket.modifyRegisterUser(registerUser);
        assertNotNull(ticket.getUser());
        assertEquals(1, registerUser.getTickets().size());
    }

    @Test
    public void shouldAddTwoTicketToUser() {
        Ticket ticket1 = new Ticket();
        ticket1.modifyId(1);
        RegisterUser registerUser1 = new RegisterUser();
        registerUser1.setLogin("user1");
        RegisterUser registerUser2 = new RegisterUser();
        registerUser2.setLogin("user2");
        ticket1.modifyRegisterUser(registerUser1);
        ticket1.modifyRegisterUser(registerUser2);
        assertEquals(0, registerUser1.getTickets().size());
        assertEquals(1, registerUser2.getTickets().size());
    }

    @Test
    public void shouldSwapTicketWindow() {
        Seance seance = TestUtil.createCinema().findSeanceById(1);
        Ticket ticket = new Ticket();
        ticket.modifyId(1);
        TicketWindow ticketWindow1 = new TicketWindow();
        ticketWindow1.modifyId(1);
        TicketWindow ticketWindow2 = new TicketWindow();
        ticketWindow2.modifyId(2);
        ticket.modifyTicketWindow(ticketWindow1);
        ticket.modifySeance(seance);
        ticket.modifyTicketWindow(ticketWindow2);
        ticket.modifySeance(seance);
        assertEquals(0, ticketWindow1.getTickets().size());
        assertEquals(1, ticketWindow2.getTickets().size());
        assertEquals(ticketWindow2, ticket.getTicketWindow());
        assertEquals(seance, ticket.getSeance());
    }
}
