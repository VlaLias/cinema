package org.sytoss.trainee.cinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.util.FileUtil;
import org.sytoss.trainee.cinema.util.TestUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TicketServiceCSVTest extends TicketServiceAbstractTest {

    @Autowired
    @Qualifier("ticketServiceCSVImpl")
    private TicketService ticketService;

    @Override
    protected TicketService getTicketService() {
        return ticketService;
    }

    @Override
    protected String getReadPath() throws IOException {
        List<String> testList = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            testList.add(i + ",\"Броненосец \"\"Потемкин\"\", часть 2\",\"13.07.2019 21:14\",\"Белый\",1,1,91.5");
        }
        return FileUtil.createTestFile(testList, ".csv").getPath();
    }

    @Override
    protected String getWritePath() {
        return "src/test/resources/ticketServiceTestResource.csv";
    }

    @Override
    protected Set<Ticket> getTickets() {
        return TestUtil.createCinema().getTicketWindows().iterator().next().getTickets();
    }

    @Override
    protected int getExpectedAmount() {
        return TestUtil.createCinema().getTicketWindows().iterator().next().getTickets().size();
    }
}
