package org.sytoss.trainee.cinema.service;

import org.jdom2.JDOMException;
import org.junit.Test;
import org.sytoss.trainee.cinema.AbstractCinemaTest;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public abstract class TicketServiceAbstractTest extends AbstractCinemaTest {

    protected abstract TicketService getTicketService();

    protected abstract String getReadPath() throws IOException;

    protected abstract String getWritePath();

    protected abstract Set<Ticket> getTickets();

    protected abstract int getExpectedAmount();

    @Test
    public void shouldRead() throws IOException, JDOMException, XmlPullParserException {
        Set<Ticket> ticketsActual = getTicketService().read(getReadPath());
        assertEquals(7, ticketsActual.size());
    }

    @Test(expected = NullPointerException.class)
    public void shouldRaiseNullPointerExceptionForRead() throws IOException {
        String path = null;
        getTicketService().read(path);
    }

    @Test
    public void shouldWrite() throws IOException {
        getTicketService().write(getTickets(), getWritePath());
        BufferedReader reader = Files.newBufferedReader(Paths.get(getWritePath()));
        int i = 0;
        while ((reader.readLine()) != null) {
            i++;
        }
        assertEquals(getExpectedAmount(), i);
    }

}
