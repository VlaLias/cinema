package org.sytoss.trainee.cinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.util.TestUtil;

import java.io.IOException;
import java.util.Set;

public class TicketServiceXPPTest extends TicketServiceAbstractTest {

    @Autowired
    @Qualifier("ticketServiceXPPImpl")
    private TicketService ticketService;

    @Override
    protected TicketService getTicketService() {
        return ticketService;
    }

    @Override
    protected String getReadPath() throws IOException {
        return "src/test/resources/ticketServiceTestResource.xml";
    }

    @Override
    protected String getWritePath() {
        return "src/test/resources/ticketServiceTestResource.xml";
    }

    @Override
    protected Set<Ticket> getTickets() {
        return TestUtil.createCinema().getTicketWindows().iterator().next().getTickets();
    }

    @Override
    protected int getExpectedAmount() {
        return 54;
    }
}
