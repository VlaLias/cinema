package org.sytoss.trainee.cinema.translator;

import org.jdom2.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.sytoss.trainee.cinema.dom.Seat;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.util.TranslatorUtil;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

@Component
public class SeatTranslator {

    @Value("${QUOTE}")
    private String quote;

    @Value("${SEPARATOR}")
    private String separator;

    @Autowired
    private TranslatorUtil translatorUtil;

    public StringBuilder toStringBuilder(Seat seat) {
        StringBuilder filmTransferObject = new StringBuilder();
        filmTransferObject.append(translatorUtil.wrapWithQuotes(TranslatorUtil.parsePhrase(String.valueOf(seat.getLine())
                , quote), quote).append(separator));
        filmTransferObject.append(translatorUtil.wrapWithQuotes(TranslatorUtil.parsePhrase(String.valueOf(seat.getPlace())
                , quote), quote));
        return filmTransferObject;
    }

    public void toElement(Seat source, Element destination) {
        Element seatElement = new Element("seat");
        seatElement.setAttribute("line", String.valueOf(source.getLine()));
        seatElement.setAttribute("place", String.valueOf(source.getPlace()));
        destination.addContent(seatElement);
    }

    public Seat toSeat(XmlPullParser xpp) throws XmlPullParserException, IOException {
        Seat seat = new Seat();
        seat.modifyLine(Integer.parseInt(xpp.getAttributeValue(null, "line")));
        seat.modifyPlace(Integer.parseInt(xpp.getAttributeValue(null, "place")));
        return seat;
    }

    public void toSerializer(Ticket ticket, XmlSerializer serializer) throws IOException {
        serializer.startTag(null, "seat")
                .attribute(null, "line", String.valueOf(ticket.getSeat().getLine()))
                .attribute(null, "place", String.valueOf(ticket.getSeat().getPlace()));
        serializer.endTag(null, "seat");
    }

    public Seat fromElement(Element seatElement) {
        Seat seat = new Seat();
        seat.modifyPlace(TranslatorUtil.parseStringToInt(seatElement.getAttribute("place").getValue()));
        seat.modifyLine(TranslatorUtil.parseStringToInt(seatElement.getAttribute("line").getValue()));
        return seat;
    }
}
