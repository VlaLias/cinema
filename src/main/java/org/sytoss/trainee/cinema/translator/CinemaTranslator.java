package org.sytoss.trainee.cinema.translator;

import org.jdom2.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.sytoss.trainee.cinema.dom.Cinema;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.dom.TicketWindow;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Component
public class CinemaTranslator {

    @Autowired
    private TicketWindowTranslator ticketWindowTranslator;

    public void toSerializer(Set<Ticket> tickets, XmlSerializer serializer) throws IOException {
        Iterator<Ticket> i = tickets.iterator();
        Ticket ticketiter = i.next();
        serializer.startTag(null, "cinema")
                .attribute(null, "name", ticketiter.getTicketWindow().getCinema().getName());

        TicketWindow ticketWindow = tickets.size() > 0 ? ticketiter.getTicketWindow() : null;
        List<Ticket> ticketWindowTickets = new ArrayList<>();
        for (Ticket ticket : tickets) {
            if (Objects.equals(ticket.getTicketWindow(), ticketWindow)) {
                ticketWindowTickets.add(ticket);
            } else {
                ticketWindowTranslator.toSerializer(ticketWindowTickets, serializer);
                ticketWindow = ticket.getTicketWindow();
                ticketWindowTickets = new ArrayList<>();
                ticketWindowTickets.add(ticket);
            }
        }
        ticketWindowTranslator.toSerializer(ticketWindowTickets, serializer);
        serializer.endTag(null, "cinema");
    }

    public Element toElement(Cinema source) {
        Element cinemaElement = new Element("cinema");
        cinemaElement.setAttribute("name", source.getName());
        return cinemaElement;
    }
}
