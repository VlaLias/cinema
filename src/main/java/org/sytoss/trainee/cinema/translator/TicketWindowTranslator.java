package org.sytoss.trainee.cinema.translator;

import org.jdom2.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.sytoss.trainee.cinema.dom.Film;
import org.sytoss.trainee.cinema.dom.Hall;
import org.sytoss.trainee.cinema.dom.Seance;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.dom.TicketWindow;
import org.sytoss.trainee.cinema.util.TicketComparator;
import org.sytoss.trainee.cinema.util.TranslatorUtil;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Component
public class TicketWindowTranslator {

    @Autowired
    private FilmTranslator filmTranslator;

    @Autowired
    private HallTranslator hallTranslator;

    @Autowired
    private SeanceTranslator seanceTranslator;

    @Autowired
    private TicketTranslator ticketTranslator;

    public void toSerializer(List<Ticket> ticketWindowTickets, XmlSerializer serializer) throws IOException {
        serializer.startTag(null, "ticketWindow")
                .attribute(null, "id", String.valueOf(ticketWindowTickets.get(0).getTicketWindow().getId()));
        Collections.sort(ticketWindowTickets, new TicketComparator());

        Film film = ticketWindowTickets.size() > 0 ? ticketWindowTickets.get(0).getSeance().getFilm() : null;
        List<Ticket> filmTickets = new ArrayList<>();
        for (Ticket ticket : ticketWindowTickets) {
            if (Objects.equals(ticket.getSeance().getFilm(), film)) {
                filmTickets.add(ticket);
            } else {
                filmTranslator.toSerializer(filmTickets, serializer);
                film = ticket.getSeance().getFilm();
                filmTickets = new ArrayList<>();
                filmTickets.add(ticket);
            }
        }
        filmTranslator.toSerializer(filmTickets, serializer);
        serializer.endTag(null, "ticketWindow");
    }

    public void toElement(TicketWindow source, Element destination) {
        Element ticketWindowElement = new Element("ticketWindow");
        ticketWindowElement.setAttribute("id", String.valueOf(source.getId()));
        destination.getChildren().add(ticketWindowElement);
        source.getTickets().stream()
                .forEach(p -> ticketTranslator.toElement(p, ticketWindowElement));
    }

    public TicketWindow toTicketWindow(XmlPullParser xpp) {
        TicketWindow ticketWindow = new TicketWindow();
        ticketWindow.modifyId(Integer.parseInt(xpp.getAttributeValue(null, "id")));
        return ticketWindow;
    }

    public TicketWindow fromElement(Element ticketWindowElement) {
        TicketWindow ticketWindow = new TicketWindow();
        ticketWindow.modifyId(TranslatorUtil.parseStringToInt(ticketWindowElement.getAttribute("id").getValue()));

        for (Element filmElement : ticketWindowElement.getChildren()) {
            Film film = filmTranslator.fromElement(filmElement);
            for (Element hallElement : filmElement.getChildren()) {
                Hall hall = hallTranslator.fromElement(hallElement);
                for (Element seanceElement : hallElement.getChildren()) {
                    Seance seance = seanceTranslator.fromElement(seanceElement, ticketWindow);
                    seance.setHall(hall);
                    seance.setFilm(film);
                    ticketWindow.addToSeances(seance);
                    seance.getTickets().stream().forEach(p -> ticketWindow.addToTickets(p));
                }
            }
        }
        return ticketWindow;
    }
}
