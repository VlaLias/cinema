package org.sytoss.trainee.cinema.translator;

import org.jdom2.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.sytoss.trainee.cinema.dom.Seance;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.dom.TicketWindow;
import org.sytoss.trainee.cinema.util.TicketComparator;
import org.sytoss.trainee.cinema.util.TranslatorUtil;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Component
public class SeanceTranslator {

    @Value("${QUOTE}")
    private String quote;

    @Value("${SEPARATOR}")
    private String separator;

    @Autowired
    private FilmTranslator filmTranslator;

    @Autowired
    private HallTranslator hallTranslator;

    @Autowired
    private TicketTranslator ticketTranslator;

    @Autowired
    private TranslatorUtil translatorUtil;

    public StringBuilder toStringBuilder(Seance seance) {
        StringBuilder seanceTransferObject = new StringBuilder();
        seanceTransferObject.append(filmTranslator.toStringBuilder(seance.getFilm()).append(separator));
        seanceTransferObject.append(translatorUtil.wrapWithQuotes(seance.getStartDate().toString("dd.MM.yyyy HH:mm"), quote)
                .append(separator));
        seanceTransferObject.append(hallTranslator.toStringBuilder(seance.getHall()).toString());
        return seanceTransferObject;
    }

    public Element toElement(Seance source, Element destination) {
        Element seanceElement = new Element("seance");
        seanceElement.setAttribute("id", String.valueOf(source.getId()));
        seanceElement.setAttribute("startDate", source.getStartDate().toString("dd.MM.yyyy HH:mm"));
        destination.addContent(seanceElement);
        return seanceElement;
    }

    public void toSerializer(List<Ticket> seanceTickets, XmlSerializer serializer) throws IOException {
        Collections.sort(seanceTickets, new TicketComparator());

        serializer.startTag(null, "seance")
                .attribute(null, "id", String.valueOf(seanceTickets.get(0).getSeance().getId()))
                .attribute(null, "startDate", seanceTickets.get(0).getSeance().getStartDate().toString("dd.MM.yyyy HH:mm"));

        for (Ticket ticket : seanceTickets) {
            ticketTranslator.toSerializer(ticket, serializer);
        }

        serializer.endTag(null, "seance");
    }

    public Seance toSeance(XmlPullParser xpp) {
        Seance seance = new Seance();
        seance.modifyId(Integer.parseInt(xpp.getAttributeValue(null, "id")));
        seance.modifyStartDate(translatorUtil.parseStringToDate(xpp.getAttributeValue(null, "startDate")));
        return seance;
    }

    public Seance fromElement(Element seanceElement, TicketWindow ticketWindow) {
        Seance seance = new Seance();
        seance.modifyId(TranslatorUtil.parseStringToInt(seanceElement.getAttribute("id").getValue()));
        seance.modifyStartDate(translatorUtil.parseStringToDate(seanceElement.getAttributeValue("startDate")));

        for (Element ticketElement : seanceElement.getChildren()) {
            Ticket ticket = ticketTranslator.fromElement(ticketElement);
            ticket.modifyTicketWindow(ticketWindow);
            ticket.modifySeance(seance);
            seance.addToTickets(ticket);
        }
        return seance;
    }
}