package org.sytoss.trainee.cinema.translator;

import org.jdom2.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.sytoss.trainee.cinema.dom.Film;
import org.sytoss.trainee.cinema.dom.Hall;
import org.sytoss.trainee.cinema.dom.RegisterUser;
import org.sytoss.trainee.cinema.dom.Seance;
import org.sytoss.trainee.cinema.dom.Seat;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.util.TranslatorUtil;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

@Component
public class TicketTranslator {

    @Value("${QUOTE}")
    private String quote;

    @Value("${SEPARATOR}")
    private String separator;

    @Autowired
    private SeatTranslator seatTranslator;

    @Autowired
    private SeanceTranslator seanceTranslator;

    @Autowired
    private FilmTranslator filmTranslator;

    @Autowired
    private HallTranslator hallTranslator;

    @Autowired
    private TicketTranslator ticketTranslator;

    @Autowired
    private RegisterUserTranslator registerUserTranslator;

    @Autowired
    private TicketWindowTranslator ticketWindowTranslator;

    @Autowired
    private TranslatorUtil translatorUtil;

    public StringBuilder toStringBuilder(Ticket ticket) {
        StringBuilder ticketTransferObject = new StringBuilder();
        ticketTransferObject.append(String.valueOf(ticket.getId()) + separator);
        ticketTransferObject.append(seanceTranslator.toStringBuilder(ticket.getSeance())
                .append(separator));
        ticketTransferObject.append(seatTranslator.toStringBuilder(ticket.getSeat())
                .append(separator));
        ticketTransferObject.append(String.valueOf(ticket.getPrice()));

        return ticketTransferObject;
    }

    public void toElement(Ticket source, Element destination) {
        Element filmElement, hallElement, seanceElement;
        if ((filmElement = TranslatorUtil.findByAttrValue(destination, "name", source.getSeance().getFilm().getName())) == null) {
            filmElement = filmTranslator.toElement(source.getSeance().getFilm(), destination);
        }
        if ((hallElement = TranslatorUtil.findByAttrValue(filmElement, "name", source.getSeance().getHall().getName())) == null) {
            hallElement = hallTranslator.toElement(source.getSeance().getHall(), filmElement);
        }
        if ((seanceElement = TranslatorUtil.findByAttrValue(hallElement, "id", String.valueOf(source.getSeance().getId()))) == null) {
            seanceElement = seanceTranslator.toElement(source.getSeance(), hallElement);
        }
        Element ticketElement = new Element("ticket");
        ticketElement.setAttribute("id", String.valueOf(source.getId()));
        ticketElement.setAttribute("price", String.valueOf(source.getPrice()));
        seatTranslator.toElement(source.getSeat(), ticketElement);
        if (source.getUser() != null) {
            registerUserTranslator.toElement(source.getUser(), ticketElement);
        }
        seanceElement.addContent(ticketElement);
    }

    public Ticket toTicket(StringBuilder ticketTransferObject) throws NumberFormatException {
        Ticket result = new Ticket();
        List<String> ticketValues = new ArrayList<>();
        StringBuffer value = new StringBuffer();
        boolean inQuotes = false;
        boolean endOfQuotes = false;
        boolean multipleQuotes = false;

        char[] ticketStringChars = ticketTransferObject.toString().toCharArray();

        for (char character : ticketStringChars) {
            if (inQuotes) {
                if (quote.equals(String.valueOf(character))) {
                    if (multipleQuotes) {
                        value.append(quote);
                        multipleQuotes = false;
                    } else {
                        inQuotes = false;
                        endOfQuotes = true;
                    }
                } else {
                    value.append(character);
                }
            } else {
                if (character == System.getProperty("line.separator").charAt(0)) {
                    break;
                }
                if (endOfQuotes && !(separator.equals(String.valueOf(character)))) {
                    value.append(quote);

                    if (!quote.equals(String.valueOf(character))) {
                        multipleQuotes = true;
                        value.append(character);
                    }
                    inQuotes = true;
                    endOfQuotes = false;
                    continue;
                }
                if (quote.equals(String.valueOf(character))) {
                    inQuotes = true;
                    continue;
                }
                if (separator.equals(String.valueOf(character))) {
                    ticketValues.add(value.toString());
                    value = new StringBuffer();
                    endOfQuotes = false;
                    continue;
                }
                value.append(character);
            }
        }
        ticketValues.add(value.toString());
        result.modifyId(Integer.parseInt(ticketValues.get(0)));
        result.modifySeance(new Seance(
                new Film(ticketValues.get(1), 1),
                translatorUtil.parseStringToDate(ticketValues.get(2)),
                new Hall(ticketValues.get(3)))
        );
        result.setSeat(new Seat(
                Integer.parseInt(ticketValues.get(4)),
                Integer.parseInt(ticketValues.get(5)))
        );
        result.modifyPrice(Double.parseDouble(ticketValues.get(6)));
        return result;
    }

    public Set<Ticket> toTickets(List<StringBuilder> ticketsStrings) throws IllegalArgumentException {
        Set<Ticket> tickets = new TreeSet<>();
        for (StringBuilder ticketString : ticketsStrings) {
            if (ticketString.toString().equals("")) {
                throw new IllegalArgumentException("One of TicketTransferObjects is empty!");
            }
            tickets.add(ticketTranslator.toTicket(ticketString));
        }
        return tickets;
    }

    public void toSerializer(Ticket ticket, XmlSerializer serializer) throws IOException {
        serializer.startTag(null, "ticket")
                .attribute(null, "id", String.valueOf(ticket.getId()))
                .attribute(null, "price", String.valueOf(ticket.getPrice()));

        seatTranslator.toSerializer(ticket, serializer);
        if (ticket.getUser() != null) {
            registerUserTranslator.toSerializer(ticket, serializer);
        }

        serializer.endTag(null, "ticket");
    }

    public Set<Ticket> fromPullParser(XmlPullParser xpp) throws XmlPullParserException, IOException {

        int eventType = xpp.getEventType();

        Set<Ticket> tickets = new TreeSet<>();
        Ticket tempTicket = new Ticket();
        while (eventType != XmlPullParser.END_DOCUMENT) {

            switch (eventType) {
                case XmlPullParser.START_TAG:

                    if (xpp.getName().equals("ticketWindow")) {
                        tempTicket.modifyTicketWindow(ticketWindowTranslator.toTicketWindow(xpp));
                    } else if (xpp.getName().equals("film")) {
                        Seance seance = new Seance();
                        Film film = filmTranslator.convertFilm(xpp);
                        seance.setFilm(film);
                        tempTicket.modifySeance(seance);
                    } else if (xpp.getName().equals("hall")) {
                        Hall hall = hallTranslator.convertHall(xpp);
                        tempTicket.getSeance().setHall(hall);
                    } else if (xpp.getName().equals("seance")) {
                        Seance tempSeance = seanceTranslator.toSeance(xpp);
                        tempTicket.modifySeance(tempSeance);
                    } else if (xpp.getName().equals("ticket")) {
                        Ticket translatedTicket = ticketTranslator.toTicket(xpp);
                        tempTicket.modifyId(translatedTicket.getId());
                        tempTicket.modifyPrice(translatedTicket.getPrice());
                    } else if (xpp.getName().equals("seat")) {
                        Seat seat = seatTranslator.toSeat(xpp);
                        tempTicket.setSeat(seat);
                    } else if (xpp.getName().equals("user")) {
                        RegisterUser registerUser = registerUserTranslator.toRegisterUser(xpp);
                        tempTicket.modifyRegisterUser(registerUser);
                    }

                    break;

                case XmlPullParser.END_TAG:
                    if (xpp.getName().equals("ticket")) {
                        Ticket ticket = new Ticket();
                        ticket.modifyId(tempTicket.getId());
                        ticket.modifyPrice(tempTicket.getPrice());
                        ticket.modifySeance(tempTicket.getSeance());
                        ticket.setSeat(tempTicket.getSeat());
                        if (tempTicket.getUser() != null) {
                            ticket.modifyRegisterUser(tempTicket.getUser());
                        }
                        ticket.modifyTicketWindow(tempTicket.getTicketWindow());
                        tickets.add(ticket);
                        tempTicket.modifyRegisterUser(new RegisterUser());
                    }
                    break;

                default:
                    break;
            }

            eventType = xpp.next();
        }

        return tickets;

    }

    private Ticket toTicket(XmlPullParser xpp) {
        Ticket ticket = new Ticket();
        ticket.modifyId(Integer.parseInt(xpp.getAttributeValue(null, "id")));
        ticket.modifyPrice(Double.parseDouble(xpp.getAttributeValue(null, "price")));
        return ticket;
    }

    public Ticket fromElement(Element ticketElement) {
        Ticket ticket = new Ticket();
        ticket.modifyId(TranslatorUtil.parseStringToInt(ticketElement.getAttributeValue("id")));
        ticket.modifyPrice(TranslatorUtil.parseStringToDouble(ticketElement.getAttributeValue("price")));
        ticket.setSeat(seatTranslator.fromElement(ticketElement.getChild("seat")));
        if (!Objects.equals(ticketElement.getChild("user"), null)) {
            ticket.modifyRegisterUser(registerUserTranslator.fromElement(ticketElement.getChild("user")));
        } else {
            ticket.modifyRegisterUser(new RegisterUser());
        }
        return ticket;
    }

}