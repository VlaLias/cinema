package org.sytoss.trainee.cinema.translator;

import org.jdom2.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.sytoss.trainee.cinema.dom.Film;
import org.sytoss.trainee.cinema.dom.Hall;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.util.TicketComparator;
import org.sytoss.trainee.cinema.util.TranslatorUtil;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Component
public class FilmTranslator {

    @Value("${QUOTE}")
    private String quote;

    @Autowired
    private HallTranslator hallTranslator;

    @Autowired
    private TranslatorUtil translatorUtil;

    public StringBuilder toStringBuilder(Film film) {
        StringBuilder filmTransferObject = new StringBuilder();
        filmTransferObject.append(translatorUtil.wrapWithQuotes(TranslatorUtil.parsePhrase(film.getName(), quote), quote));
        return filmTransferObject;
    }

    public void toSerializer(List<Ticket> filmTickets, XmlSerializer serializer) throws IOException {
        serializer.startTag(null, "film")
                .attribute(null, "name", filmTickets.get(0).getSeance().getFilm().getName());
        Collections.sort(filmTickets, new TicketComparator());

        Hall hall = filmTickets.size() > 0 ? filmTickets.get(0).getSeance().getHall() : null;
        List<Ticket> hallTickets = new ArrayList<>();
        for (Ticket ticket : filmTickets) {
            if (Objects.equals(ticket.getSeance().getHall(), hall)) {
                hallTickets.add(ticket);
            } else {
                hallTranslator.toSerializer(hallTickets, serializer);
                hall = ticket.getSeance().getHall();
                hallTickets = new ArrayList<>();
                hallTickets.add(ticket);
            }
        }
        hallTranslator.toSerializer(hallTickets, serializer);
        serializer.endTag(null, "film");
    }

    public Film convertFilm(XmlPullParser xpp) {
        Film film = new Film();
        film.setName(xpp.getAttributeValue(null, "name"));
        return film;
    }

    public Element toElement(Film source, Element destination) {
        Element filmElement = new Element("film");
        filmElement.setAttribute("name", source.getName());
        destination.addContent(filmElement);
        return filmElement;
    }

    public Film fromElement(Element filmElement) {
        Film film = new Film();
        film.setName(filmElement.getAttributeValue("name"));
        return film;
    }
}
