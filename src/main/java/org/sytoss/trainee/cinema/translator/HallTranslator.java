package org.sytoss.trainee.cinema.translator;

import org.jdom2.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.sytoss.trainee.cinema.dom.Hall;
import org.sytoss.trainee.cinema.dom.Seance;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.util.TicketComparator;
import org.sytoss.trainee.cinema.util.TranslatorUtil;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Component
public class HallTranslator {

    @Value("${QUOTE}")
    private String quote;

    @Autowired
    private SeanceTranslator seanceTranslator;

    @Autowired
    private TranslatorUtil translatorUtil;

    public StringBuilder toStringBuilder(Hall hall) {
        StringBuilder filmTransferObject = new StringBuilder();
        if (hall.getName() != null) {
            filmTransferObject.append(translatorUtil.wrapWithQuotes(TranslatorUtil.parsePhrase(hall.getName(), quote), quote));
        }
        return filmTransferObject;
    }

    public void toSerializer(List<Ticket> hallTickets, XmlSerializer serializer) throws IOException {
        serializer.startTag(null, "hall")
                .attribute(null, "name", hallTickets.get(0).getSeance().getHall().getName());

        Collections.sort(hallTickets, new TicketComparator());

        Seance seance = hallTickets.size() > 0 ? hallTickets.get(0).getSeance() : null;
        List<Ticket> seanceTickets = new ArrayList<>();
        for (Ticket ticket : hallTickets) {
            if (Objects.equals(ticket.getSeance(), seance)) {
                seanceTickets.add(ticket);
            } else {
                seanceTranslator.toSerializer(seanceTickets, serializer);
                seance = ticket.getSeance();
                seanceTickets = new ArrayList<>();
                seanceTickets.add(ticket);
            }
        }
        seanceTranslator.toSerializer(seanceTickets, serializer);
        serializer.endTag(null, "hall");
    }

    public Hall convertHall(XmlPullParser xpp) {
        Hall hall = new Hall();
        hall.setName(xpp.getAttributeValue(null, "name"));
        return hall;
    }

    public Element toElement(Hall source, Element destination) {
        Element hallElement = new Element("hall");
        hallElement.setAttribute("name", source.getName());
        destination.addContent(hallElement);
        return hallElement;
    }

    public Hall fromElement(Element hallElement) {
        Hall hall = new Hall();
        hall.setName(hallElement.getAttribute("name").getValue());
        return hall;
    }
}
