package org.sytoss.trainee.cinema.translator;

import org.jdom2.Element;
import org.springframework.stereotype.Component;
import org.sytoss.trainee.cinema.dom.RegisterUser;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

@Component
public class RegisterUserTranslator {

    public void toSerializer(Ticket ticket, XmlSerializer serializer) throws IOException {
        serializer.startTag(null, "user")
                .attribute(null, "id", String.valueOf(ticket.getUser().getId()))
                .attribute(null, "login", String.valueOf(ticket.getUser().getLogin()));
        serializer.endTag(null, "user");
    }

    public void toElement(RegisterUser source, Element destination) {
        Element userElement = new Element("user");
        userElement.setAttribute("id", String.valueOf(source.getId()));
        userElement.setAttribute("login", String.valueOf(source.getLogin()));
        destination.addContent(userElement);
    }

    public RegisterUser toRegisterUser(XmlPullParser xpp) {
        RegisterUser registerUser = new RegisterUser();
        registerUser.setLogin(xpp.getAttributeValue(null, "login"));
        return registerUser;
    }

    public RegisterUser fromElement(Element element) {
        RegisterUser registerUser = new RegisterUser();
        registerUser.setLogin(element.getAttribute("login").getValue());
        return registerUser;
    }
}
