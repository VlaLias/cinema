package org.sytoss.trainee.cinema.dom;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

@NoArgsConstructor
@ToString(exclude = "seats")
public class Hall {

    @Getter
    private int id;
    @Getter
    @Setter
    private String name;
    private Set<Seat> seats = new TreeSet<>();

    public Hall(String name) {
        this();
        this.setName(name);
    }

    public Set<Seat> getSeats() {
        return Collections.unmodifiableSet(seats);
    }

    public void setSeats(Set<Seat> seats) {
        this.seats = seats;
    }
}
