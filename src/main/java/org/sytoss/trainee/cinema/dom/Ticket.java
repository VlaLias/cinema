package org.sytoss.trainee.cinema.dom;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class Ticket implements Comparable {

    @Getter
    private int id;
    @Getter
    private double price;
    @Getter
    @Setter
    private Seat seat;
    @Getter
    private Seance seance;
    @Getter
    private RegisterUser user;
    @Getter
    private TicketWindow ticketWindow;

    public void modifyId(int id) {
        if (id >= 0) {
            this.id = id;
        } else {
            throw new IllegalArgumentException("Ticket id can't be negative");
        }
    }

    public void modifyPrice(double price) {
        if (price > 0) {
            this.price = price;
        } else {
            throw new IllegalArgumentException("Price can't be negative or 0");
        }
    }

    public void modifySeance(Seance seance) {
        if (this.seance != null) {
            this.seance.deleteTicket(this);
        }
        this.seance = seance;
        if (!seance.getTickets().contains(this)) {
            seance.addToTickets(this);
        }
    }

    public void clearSeance() {
        this.seance = null;
    }

    public void modifyRegisterUser(RegisterUser user) {
        if (this.user != null) {
            this.user.deleteTicket(this);
        }
        this.user = user;
        user.addToTickets(this);
    }

    public void clearRegisterUser() {
        this.user = null;
    }

    public void modifyTicketWindow(TicketWindow ticketWindow) {
        if (this.ticketWindow != null) {
            this.ticketWindow.deleteTicket(this);
        }
        this.ticketWindow = ticketWindow;
        if (!ticketWindow.getTickets().contains(this)) {
            ticketWindow.addToTickets(this);
        }
    }

    public void clearTicketWindow() {
        this.ticketWindow = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Ticket ticket = (Ticket) o;

        if (id != ticket.id) {
            return false;
        }
        return seance != null ? seance.equals(ticket.seance) : ticket.seance == null;
    }

    @Override
    public int compareTo(Object o) {
        Ticket ticket = (Ticket) o;
        return id - ticket.getId();
    }
}
