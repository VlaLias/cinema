package org.sytoss.trainee.cinema.dom;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.joda.time.DateTime;
import org.sytoss.trainee.cinema.exception.ObjectNotFoundException;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

@NoArgsConstructor
@ToString
public class TicketWindow implements Comparable {

    @Getter
    private int id;
    @Getter
    private Cinema cinema;
    private Set<Seance> seances = new TreeSet<>();
    private Set<Ticket> tickets = new TreeSet<>();

    public void modifyId(int id) {
        if (id <= 0) {
            throw new IllegalArgumentException("Ticket Window ID should be non-negative.");
        }
        this.id = id;
    }

    public void modifyCinema(Cinema cinema) {
        if (this.cinema != null && this.cinema.getId() != cinema.getId()) {
            this.cinema.deleteTicketWindow(this);
        }
        this.cinema = cinema;
        if (!cinema.getTicketWindows().contains(this)) {
            cinema.addToTicketWindows(this);

        }
    }

    public Set<Ticket> getTickets() {
        return Collections.unmodifiableSet(this.tickets);
    }

    public void addToTickets(Ticket ticket) {
        if (ticket == null) {
            throw new IllegalArgumentException("Ticket cannot be null.");
        }

        tickets.add(ticket);
        if (ticket.getTicketWindow() == null || ticket.getTicketWindow().getId() != this.getId()) {
            ticket.modifyTicketWindow(this);
        }
    }

    public Set<Seance> getSeances() {
        return Collections.unmodifiableSet(this.seances);
    }

    public void addToSeances(Seance seance) {
        if (isExistedSeance(seance)) {
            throw new IllegalArgumentException("Cannot add already existed seance");
        }
        if (seance == null) {
            throw new IllegalArgumentException("Seance cannot be null");
        } else {
            seances.add(seance);
        }
    }

    public void deleteTicket(Ticket ticket) {
        if (!checkTicketExistence(ticket)) {
            throw new IllegalArgumentException("Cannot delete unexisted ticket");
        }
        if (!checkTicketUsability(ticket)) {
            throw new IllegalArgumentException("Cannot delete overdue ticket");
        }
        removeTicketFromTicketWindow(ticket);
        if (ticket.getUser() != null) {
            ticket.getUser().deleteTicket(ticket);
        }
        ticket.getSeance().deleteTicket(ticket);
    }

    public Double calculateIncomeBySeance(Seance seance) {
        Double result = 0.0;
        if (isExistedSeance(seance)) {
            for (Ticket ticket : seance.getTickets()) {
                result += ticket.getPrice();
            }
        } else {
            throw new IllegalArgumentException("Cannot calculate income from unexisted seance");
        }
        return result;
    }

    public Double calculateTotalIncome() {
        Double result = 0.0;
        for (Ticket ticket : this.getTickets()) {
            result += ticket.getPrice();
        }
        return result;
    }

    public boolean isExistedSeance(Seance seance) {

        for (Seance sean : this.getSeances()) {
            if (sean.equals(seance)) {
                return true;
            }
        }
        return false;
    }

    public Seance findSeanceById(int seanceId) {
        for (Seance seance : this.getSeances()) {
            if (seance.getId() == (seanceId)) {
                return seance;
            }
        }
        throw new ObjectNotFoundException("Cannot find seance with id:" + seanceId);
    }

    public boolean checkTicketExistence(Ticket ticket) {
        boolean result = false;
        for (Ticket tick : tickets) {
            if (tick.equals(ticket)) {
                result = true;
            }
        }
        return result;
    }

    public boolean checkTicketUsability(Ticket ticket) {
        boolean result = false;
        if (ticket.getSeance().getStartDate().isAfter(new DateTime())) {
            result = true;
        }
        return result;
    }

    private void removeTicketFromTicketWindow(Ticket ticket) {
        this.tickets.remove(ticket);
        ticket.clearTicketWindow();
    }

    public void calculatePrice(Seance seance) {
        calculatePrice(seance, null);
    }

    public double calculatePrice(Seance seance, RegisterUser regUser) {
        Double price = seance.getPrice();
        if (regUser != null) {
            price = price - (price * regUser.getDiscount() / 100);
        }
        return price;
    }

    public Ticket createTicket(int id, Seance seance, Seat seat) {
        return createTicket(id, seance, seat, null);
    }

    public Ticket createTicket(int id, Seance seance, Seat seat, String login) {
        if (seance.checkSeat(seat)) {
            Ticket ticket = new Ticket();
            RegisterUser registerUser;
            try {
                if (login == null) {
                    throw new ObjectNotFoundException();
                }
                registerUser = cinema.findRegisterUser(login);

            } catch (ObjectNotFoundException o) {
                registerUser = null;
            }
            double price;
            ticket.modifyId(id);
            ticket.setSeat(seat);
            ticket.modifySeance(seance);
            ticket.modifyTicketWindow(this);
            price = calculatePrice(seance, registerUser);
            ticket.modifyPrice(price);
            seance.addToTickets(ticket);
            tickets.add(ticket);
            if (login != null) {
                registerUser.addToTickets(ticket);
                ticket.modifyRegisterUser(registerUser);
            }

            return ticket;
        } else {
            throw new IllegalArgumentException("Seat is reserved");
        }
    }

    @Override
    public int compareTo(Object obj) {
        TicketWindow ticketWindow = (TicketWindow) obj;
        return id - ticketWindow.getId();
    }
}
