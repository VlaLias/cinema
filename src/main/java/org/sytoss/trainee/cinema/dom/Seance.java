package org.sytoss.trainee.cinema.dom;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.sytoss.trainee.cinema.exception.ObjectNotFoundException;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

@NoArgsConstructor
@ToString(exclude = "tickets")
@Slf4j
public class Seance implements Comparable {

    private static final long MINUTE_IN_MILLIS = 60000;

    @Getter
    private int id;
    @Getter
    @Setter
    private Film film;
    @Getter
    private Double price;
    @Getter
    @Setter
    private Hall hall;
    @Getter
    private DateTime startDate;
    private Set<Ticket> tickets = new TreeSet<>();

    public Seance(Film film, DateTime startDate, Hall hall) {
        this();
        this.setFilm(film);
        this.modifyStartDate(startDate);
        this.setHall(hall);
    }

    public void modifyId(int id) {
        if (id >= 0) {
            this.id = id;
        } else {
            throw new IllegalArgumentException("Seance ID should be non-negative.");
        }
    }

    public void modifyPrice(Double price) {
        if (price > 0) {
            this.price = price;
        } else {
            throw new IllegalArgumentException("Price should be non-negative.");
        }

    }

    public void modifyStartDate(DateTime date) {
        if (date.isBeforeNow()) {
            throw new IllegalArgumentException("Date has passed");
        }
        this.startDate = date;
    }

    public Set<Ticket> getTickets() {
        return Collections.unmodifiableSet(tickets);
    }

    public void addToTickets(Ticket ticket) {
        log.trace("Start adding to tickets");
        if (ticket == null) {
            log.warn("Ticket cannot be null");
            throw new IllegalArgumentException("Ticket cannot be null.");
        }
        tickets.add(ticket);
        if (!ticket.getSeance().equals(this)) {
            ticket.modifySeance(this);
            log.trace("Tickets was successfully added");
        }
    }

    public void deleteTicket(Ticket ticket) {
        log.trace("Start deleting ticket");
        if (!tickets.contains(ticket)) {
            log.warn("Cannot delete unexisted ticket");
            throw new IllegalArgumentException("Cannot delete unexisted ticket");
        } else {
            this.tickets.remove(ticket);
            ticket.clearSeance();
        }
        log.trace("Deleting ticket was successful");
    }

    public boolean checkSeat(Seat seat) {
        log.trace("Start checking seat");
        boolean isSeatExists = false;
        for (Seat mySeat : this.hall.getSeats()) {
            if (mySeat.getPlace() == seat.getPlace() && mySeat.getLine() == seat.getLine()) {
                isSeatExists = true;
                break;
            }
        }
        if (!isSeatExists) {
            log.warn("Seat does not exist");
            throw new ObjectNotFoundException("Seat does not exist.");
        }
        Set<Seat> reservedSeat = new TreeSet<>();
        for (Ticket ticket : this.getTickets()) {
            reservedSeat.add(ticket.getSeat());
        }
        for (Seat someSeat : reservedSeat) {
            if (seat.getLine() == someSeat.getLine() && seat.getPlace() == someSeat.getPlace()) {
                return false;
            }
        }
        log.trace("Seat was found");
        return true;
    }

    public long getEndDate() {
        return (startDate.plus(film.getDuration() * MINUTE_IN_MILLIS)).getMillis();

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Seance seance = (Seance) o;
        if (id != seance.id) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(Object o) {
        Seance seance = (Seance) o;
        return id - seance.getId();
    }
}
