package org.sytoss.trainee.cinema.dom;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

@ToString(exclude = {"tickets"})
public class RegisterUser {

    @Getter
    private int id;
    @Getter
    private int discount;
    @Getter
    @Setter
    private String login;
    private Set<Ticket> tickets = new TreeSet<>();

    public void modifyDiscount(int discount) {
        if (discount >= 0) {
            this.discount = discount;
        } else {
            throw new IllegalArgumentException("Discount should be non-negative");
        }
    }

    public Set<Ticket> getTickets() {
        return Collections.unmodifiableSet(tickets);
    }

    public void addToTickets(Ticket ticket) {
        if (ticket != null) {
            this.tickets.add(ticket);
            if (ticket.getUser() == null) {
                ticket.modifyRegisterUser(this);
            }
            if (ticket.getUser().getId() != this.getId()) {
                ticket.modifyRegisterUser(this);
            }
        } else {
            throw new IllegalArgumentException("Ticket can't be null");

        }
    }

    public void deleteTicket(Ticket ticket) {
        if (!tickets.contains(ticket)) {
            throw new IllegalArgumentException("Cannot delete unexisted ticket");
        } else {
            this.tickets.remove(ticket);
            ticket.clearRegisterUser();
        }
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj == null) {
            result = false;
        }
        if (obj == this) {
            result = true;
        }
        if (obj.getClass() == this.getClass()) {
            RegisterUser registerUser = (RegisterUser) obj;
            if (registerUser.getId() == this.getId() || registerUser.getLogin() == this.getLogin()) {
                result = true;
            }
        }
        return result;
    }
}
