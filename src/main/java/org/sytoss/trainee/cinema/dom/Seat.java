package org.sytoss.trainee.cinema.dom;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
@ToString
public class Seat implements Comparable<Seat> {

    @Getter
    private int id;
    @Getter
    private int line;
    @Getter
    private int place;

    public Seat(int line, int place) {
        this();
        this.modifyLine(line);
        this.modifyPlace(place);
    }

    public void modifyPlace(int place) {
        if (place > 0) {
            this.place = place;
        } else {
            throw new IllegalArgumentException("Place can't be negative or zero");
        }
    }

    public void modifyLine(int line) {
        if (line > 0) {
            this.line = line;
        } else {
            throw new IllegalArgumentException("Line can't be negative or zero");
        }
    }

    @Override
    public int compareTo(@NotNull Seat o) {
        return this.place - o.place;
    }
}
