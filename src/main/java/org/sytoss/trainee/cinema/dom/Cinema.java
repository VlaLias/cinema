package org.sytoss.trainee.cinema.dom;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.sytoss.trainee.cinema.exception.ObjectNotFoundException;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

@Slf4j

@NoArgsConstructor
@ToString(exclude = {"id", "ticketWindows"})
public class Cinema {

    private static final long MINUTE_IN_MILLIS = 60000;
    private static final long BREAK_DURATION = 10;

    @Getter
    private int id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String address;

    private Set<RegisterUser> users = new HashSet<>();
    private Set<Film> films = new HashSet<>();
    private Set<Hall> halls = new HashSet<>();
    private Set<Seance> seances = new TreeSet<>();
    private Set<TicketWindow> ticketWindows = new TreeSet<>();

    @Getter
    @Setter
    private Schedule schedule = new Schedule();

    public void modifyId(int id) {
        if (id >= 0) {
            this.id = id;
        }
    }

    public Set<Film> getFilms() {
        return Collections.unmodifiableSet(films);
    }

    public Set<Hall> getHalls() {
        return Collections.unmodifiableSet(halls);
    }

    public Set<Seance> getSeances() {
        return Collections.unmodifiableSet(seances);
    }

    public void addToSeances(Seance seance) {

        log.trace("Start adding seance");
        if (seance == null) {
            log.warn("Seance cannot be null");
            throw new IllegalArgumentException("Seance cannot be null");
        } else {
            log.trace("Successful adding to seance");
            seances.add(seance);
        }
    }

    public Set<TicketWindow> getTicketWindows() {
        return Collections.unmodifiableSet(ticketWindows);
    }

    public void addToTicketWindows(TicketWindow ticketWindow) {
        log.trace("Start adding to ticket Window");
        if (ticketWindow != null) {
            this.ticketWindows.add(ticketWindow);
            if (ticketWindow.getCinema() != this || ticketWindow.getCinema() == null) {
                ticketWindow.modifyCinema(this);
            }
        } else {
            log.warn("Ticket window can't be null!");
            throw new IllegalArgumentException("Ticket window can't be null!");
        }
        log.trace("Successful adding to Ticket Window");
    }

    public Set<RegisterUser> getRegisterUsers() {
        return Collections.unmodifiableSet(this.users);
    }

    public void setRegisterUsers(Set<RegisterUser> registerUsers) {
        this.users = registerUsers;
    }

    public void addToRegisterUsers(RegisterUser registerUser) {
        log.trace("Start adding to Register Users");
        if (registerUser != null) {
            users.add(registerUser);
        } else {
            log.warn("Cannot add this user because it is null");
            throw new IllegalArgumentException("Cannot add this user because it is null");
        }
        log.trace("Successful adding to Register Users");
    }

    public RegisterUser findRegisterUser(String login) {
        log.trace("Start searching Register user");
        for (RegisterUser myUser : getRegisterUsers()) {
            if (myUser.getLogin().equals(login)) {
                log.trace("Register User has found");
                return myUser;
            }
        }
        log.warn("Couldn't find user with such login: " + login);
        throw new ObjectNotFoundException("Couldn't find user with such login: " + login);
    }

    public void addToHalls(Hall hall) {
        log.trace("Start adding to hall");
        if (hall != null) {
            try {
                findHallByName(hall.getName());
                log.warn("Hall already exists");
                throw new IllegalArgumentException("Hall: " + hall + " already exists");
            } catch (ObjectNotFoundException e) {
                log.warn("Object Not Found");
                this.halls.add(hall);
            }
        } else {
            log.warn("Cannot add this hall because it is null");
            throw new IllegalArgumentException("Cannot add this hall because it is null");
        }
        log.trace("Successful adding to hall");
    }

    public RegisterUser createNewUser(int id, String login, int discount) {
        log.trace("Start creating new user");
        try {
            findRegisterUser(login);
            log.warn("User with such login already registered");
            throw new IllegalArgumentException("User with such login already registered");
        } catch (ObjectNotFoundException e) {
            log.warn("Object not found");
            RegisterUser registerUser = new RegisterUser();
            registerUser.setLogin(login);
            registerUser.modifyDiscount(discount);
            addToRegisterUsers(registerUser);
            log.trace("Creating new user was successful");
            return registerUser;
        }

    }

    public TicketWindow createTicketWindow(int id) {
        log.trace("Start creating ticket window ");
        if (id < 0) {
            log.warn("Id should be non-negative");
            throw new IllegalArgumentException("Id should be non-negative");
        }
        TicketWindow ticketWindow = new TicketWindow();
        ticketWindow.modifyId(id);
        this.addToTicketWindows(ticketWindow);
        log.trace("Creating ticket window was successful");
        return ticketWindow;
    }

    public Film createFilm(String name, int duration) {
        log.trace("Start creating film");
        if (name == null) {
            log.warn("Name of film cannot be null");
            throw new IllegalArgumentException("Name of film cannot be null");
        }
        if (duration <= 0) {
            log.warn("Duration shouldn't be less than 1 minute");
            throw new IllegalArgumentException("Duration shouldn't be less than 1 minute");
        }
        try {
            return findFilmByName(name);
        } catch (ObjectNotFoundException e) {
            log.warn("Film not found");
            Film result = new Film();
            result.setName(name);
            result.modifyDuration(duration);
            films.add(result);
            log.trace("Creating film was successful");
            return result;
        }
    }

    public Film findFilmByName(String name) {
        log.trace("Start searching film");
        for (Film film : films) {
            if (film.getName().equals(name)) {
                log.trace("Film was found");
                return film;
            }
        }
        log.warn("Film not found");
        throw new ObjectNotFoundException("Film: " + name + " not found");
    }

    public Hall findHallByName(String name) {
        log.trace("Start searching hall");
        for (Hall hall : halls) {
            if (hall.getName().equals(name)) {
                log.trace("Hall was found");
                return hall;
            }
        }
        log.warn("Hall not found");
        throw new ObjectNotFoundException("Hall: " + name + " not found");
    }

    public Seance findSeanceById(int id) {
        log.trace("Start searching seance by Id");
        for (Seance seance : seances) {
            if (seance.getId() == id) {
                log.trace("Seance was found");
                return seance;
            }
        }
        log.warn("Seance with this id not found");
        throw new ObjectNotFoundException("Seance: " + id + " not found");
    }

    private boolean checkFreeTime(Hall hall, DateTime date, Film film) {
        log.trace("Start checking free time");
        Set<Seance> seancesInCurrentHall = new HashSet<>();
        for (Seance seance : seances) {
            if (seance.getHall().equals(hall) && seance.getStartDate().equals(date)) {
                seancesInCurrentHall.add(seance);
            }
        }
        long lastFilmMaxEndTime = date.getMillis() - BREAK_DURATION * MINUTE_IN_MILLIS;
        long nextFilmStartTime = (date.getMillis() + ((film.getDuration() + BREAK_DURATION) * MINUTE_IN_MILLIS));

        for (Seance seance : seancesInCurrentHall) {
            if (date.equals(seance.getStartDate())) {
                return false;
            }
            if ((lastFilmMaxEndTime < seance.getEndDate() && seance.getStartDate().getMillis() < date.getMillis()) ||
                    (seance.getStartDate().getMillis() < nextFilmStartTime && seance.getStartDate().getMillis() > date.getMillis())) {
                return false;
            }
        }
        log.trace("Free time was found");
        return true;
    }

    public Seance createSeance(int id, String hallName, String filmName, DateTime dateSeance, double price) {
        log.trace("Start creating seance");
        if (dateSeance.isBeforeNow()) {
            log.warn("Date has passed");
            throw new IllegalArgumentException("Date: " + dateSeance + " has passed");
        }
        if (price < 0) {
            log.warn("Price cannot be negative");
            throw new IllegalArgumentException("Price cannot be negative");
        }

        Seance result = new Seance();
        Film film = findFilmByName(filmName);
        Hall hall = findHallByName(hallName);
        if (!checkFreeTime(hall, dateSeance, film)) {
            log.warn("Cannot set seance in current time");
            throw new IllegalArgumentException("Cannot set seance in current time");
        }
        if (!schedule.validateSeance(dateSeance)) {
            log.warn("Can't set seance with such date to current schedule");
            throw new IllegalArgumentException("Can't set seance with such date to current schedule");
        }
        result.modifyId(id);
        result.setFilm(film);
        result.setHall(hall);
        result.modifyStartDate(dateSeance);
        result.modifyPrice(price);
        seances.add(result);
        schedule.addToSeances(result);
        log.trace("Creating seance was successful");
        return result;
    }

    public Schedule createSchedule(DateTime startDate, DateTime endDate) {
        log.trace("Start creating schedule");
        Schedule result = new Schedule();
        result.setStartDate(startDate);
        result.setEndDate(endDate);
        if (this.schedule.getStartDate() != null) {
            if (this.schedule.getEndDate().isAfter(startDate.getMillis())) {
                log.warn("New schedule cannot start before the end of previous schedule");
                throw new IllegalArgumentException("New schedule cannot start before the end of previous schedule");
            }
        }
        if (startDate.isAfter(endDate.getMillis())) {
            log.warn("Start date cannot be after end date schedule");
            throw new IllegalArgumentException("Start date cannot be after end date schedule");
        }
        log.trace("Schedule was successfully created");
        return result;
    }

    public void deleteTicketWindow(TicketWindow ticketWindow) {
        log.trace("Start deleting ticket window");
        if (this.getTicketWindows().contains(ticketWindow)) {
            this.ticketWindows.remove(ticketWindow);
            log.trace("Ticket window was successfully deleted");
        }
    }
}