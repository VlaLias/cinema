package org.sytoss.trainee.cinema.dom;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@ToString
public class Film {

    @Getter
    private int id;
    @Getter
    @Setter
    private String name;
    @Getter
    private int duration;

    public Film(String name, int duration) {
        this();
        this.setName(name);
        this.modifyDuration(duration);
    }

    public void modifyDuration(int duration) {
        if (duration > 0) {
            this.duration = duration;
        } else {
            throw new IllegalArgumentException("Duration can't be less than 0");
        }
    }
}
