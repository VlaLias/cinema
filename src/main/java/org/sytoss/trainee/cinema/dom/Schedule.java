package org.sytoss.trainee.cinema.dom;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

@Slf4j
public class Schedule {

    @Getter
    private int id;
    @Getter
    @Setter
    private DateTime startDate;
    @Getter
    @Setter
    private DateTime endDate;
    private Set<Seance> seances = new TreeSet<>();

    public Set<Seance> getSeances() {
        return Collections.unmodifiableSet(seances);
    }

    public void addToSeances(Seance seance) {
        log.trace("Start adding to seances");
        if (seance != null) {
            seances.add(seance);
        } else {
            log.warn("Seance can't be null");
            throw new IllegalArgumentException("Seance can't be null");
        }
        log.trace("Seance was successfully added");
    }

    public boolean validateSeance(DateTime seanceStartDate) {
        if (!this.endDate.isBefore(seanceStartDate)) {
            return true;
        } else {
            return false;
        }
    }
}
