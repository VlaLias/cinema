package org.sytoss.trainee.cinema.service.csv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sytoss.trainee.cinema.connector.FileSystemConnector;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.service.TicketService;
import org.sytoss.trainee.cinema.translator.TicketTranslator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class TicketServiceCSVImpl implements TicketService {

    @Autowired
    private TicketTranslator ticketTranslator;

    @Override
    public Set<Ticket> read(String path) throws IOException {
        FileSystemConnector connector = new FileSystemConnector();
        List<StringBuilder> ticketsStrings = connector.loadCSV(new File(path));

        return ticketTranslator.toTickets(ticketsStrings);
    }

    @Override
    public void write(Set<Ticket> tickets, String pathToFile) throws IOException {
        List<StringBuilder> ticketTransferObjects = new ArrayList<>();
        for (Ticket ticket : tickets) {
            ticketTransferObjects.add(ticketTranslator.toStringBuilder(ticket));
        }
        FileSystemConnector ticketConnector = new FileSystemConnector();
        ticketConnector.saveCSV(ticketTransferObjects, pathToFile);
    }
}
