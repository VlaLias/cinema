package org.sytoss.trainee.cinema.service;

import org.sytoss.trainee.cinema.dom.Ticket;

import java.io.IOException;
import java.util.Set;

public interface TicketService {

    Set<Ticket> read(String pathToFile) throws IOException;

    void write(Set<Ticket> tickets, String pathToFile) throws IOException;
}
