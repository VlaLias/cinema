package org.sytoss.trainee.cinema.service.xpp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sytoss.trainee.cinema.connector.FileSystemConnector;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.service.TicketService;
import org.sytoss.trainee.cinema.translator.CinemaTranslator;
import org.sytoss.trainee.cinema.translator.TicketTranslator;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

@Service
public class TicketServiceXPPImpl implements TicketService {

    @Autowired
    private TicketTranslator ticketTranslator;

    @Autowired
    private CinemaTranslator cinemaTranslator;

    @Override
    public Set<Ticket> read(String path) throws IOException {
        FileSystemConnector ticketConnector = new FileSystemConnector();
        try {
            XmlPullParser xpp = ticketConnector.loadXML(path);
            return ticketTranslator.fromPullParser(xpp);
        } catch (XmlPullParserException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void write(Set<Ticket> tickets, String pathToFile) throws IOException {
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance(System.getProperty(XmlPullParserFactory.PROPERTY_NAME), null);
            XmlSerializer serializer = factory.newSerializer();
            serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-indentation", "    ");
            serializer.setProperty("http://xmlpull.org/v1/doc/properties.html#serializer-line-separator", "\n");
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            serializer.setOutput(new PrintWriter(outputStream));
            serializer.startDocument("UTF-8", false);
            serializer.text("\n");
            cinemaTranslator.toSerializer(tickets, serializer);
            serializer.endDocument();
            new FileSystemConnector().saveToFile(pathToFile, outputStream.toByteArray());
        } catch (XmlPullParserException e) {
            throw new IOException(e);
        }
    }

}
