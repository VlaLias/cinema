package org.sytoss.trainee.cinema.service.jdom;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sytoss.trainee.cinema.connector.FileSystemConnector;
import org.sytoss.trainee.cinema.dom.Ticket;
import org.sytoss.trainee.cinema.dom.TicketWindow;
import org.sytoss.trainee.cinema.service.TicketService;
import org.sytoss.trainee.cinema.translator.CinemaTranslator;
import org.sytoss.trainee.cinema.translator.TicketWindowTranslator;
import org.sytoss.trainee.cinema.util.TranslatorUtil;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Service
public class TicketServiceJDOMImpl implements TicketService {

    @Autowired
    private TicketWindowTranslator ticketWindowTranslator;

    @Autowired
    private CinemaTranslator cinemaTranslator;

    @Override
    public Set<Ticket> read(String pathToFile) throws IOException {
        Set<Ticket> translateXMLTickets = new HashSet<>();
        try {
            Element rootElement = new FileSystemConnector().loadXMLFile(pathToFile);
            for (Element ticketWindowElement : rootElement.getChildren()) {
                TicketWindow ticketWindow = ticketWindowTranslator.fromElement(ticketWindowElement);
                translateXMLTickets.addAll(ticketWindow.getTickets());
            }
        } catch (JDOMException e) {
            throw new IOException(e);
        }
        return translateXMLTickets;
    }

    @Override
    public void write(Set<Ticket> tickets, String pathToFile) throws IOException {
        Element rootElement = cinemaTranslator.toElement(tickets.iterator().next().getTicketWindow().getCinema());
        Document document = new Document(rootElement);
        tickets.stream()
                .filter(TranslatorUtil.distinctByKey(p -> p.getTicketWindow().getId()))
                .forEach(p -> ticketWindowTranslator.toElement(p.getTicketWindow(), rootElement));
        FileSystemConnector ticketConnector = new FileSystemConnector();
        ticketConnector.saveToXmlWithJdom(document, pathToFile);
    }

}
