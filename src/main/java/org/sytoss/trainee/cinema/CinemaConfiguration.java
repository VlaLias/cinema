package org.sytoss.trainee.cinema;

import liquibase.integration.spring.SpringLiquibase;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.sytoss.trainee.cinema.dto.CinemaDto;
import org.sytoss.trainee.cinema.dto.FilmDto;
import org.sytoss.trainee.cinema.dto.HallDto;
import org.sytoss.trainee.cinema.dto.SeanceDto;
import org.sytoss.trainee.cinema.dto.SeatDto;
import org.sytoss.trainee.cinema.dto.TicketDto;
import org.sytoss.trainee.cinema.dto.TicketWindowDto;
import org.sytoss.trainee.cinema.dto.UserDto;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = {"org.sytoss.trainee.cinema", "org.sytoss.trainee.cinema.connector.dao.hibernate",
        "org.sytoss.trainee.cinema.connector.dao.mybatis", "org.sytoss.trainee.cinema.connector.dao"})
@PropertySource({"classpath:db.properties"})
@MapperScan("org.sytoss.trainee.cinema.connector.dao")
public class CinemaConfiguration {

    @Autowired
    private Environment env;

    @Bean
    public SpringLiquibase liquibase() {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource());
        liquibase.setChangeLog("classpath:liquibase/db-changelog-master.xml");
        liquibase.setContexts("development, production");
        return liquibase;
    }

    @Bean
    public DataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(env.getProperty("datasource.driverClassName"));
        dataSource.setUrl(env.getProperty("datasource.url"));
        dataSource.setUsername(env.getProperty("datasource.username"));
        dataSource.setPassword(env.getProperty("datasource.password"));
        return dataSource;
    }

    @Bean
    public LocalSessionFactoryBean getSessionFactory() {
        LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
        factoryBean.setDataSource(dataSource());
        factoryBean.setHibernateProperties(hibernateProperties());
        factoryBean.setAnnotatedClasses(CinemaDto.class, HallDto.class, FilmDto.class,
                SeatDto.class, TicketWindowDto.class, UserDto.class, SeanceDto.class, TicketDto.class);
        return factoryBean;
    }


    @Bean
    public SqlSessionFactory getSqlSessionFactory() {
        SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setTypeAliases(new Class[]{CinemaDto.class, HallDto.class, FilmDto.class,
                SeatDto.class, TicketWindowDto.class, UserDto.class, SeanceDto.class, TicketDto.class});
        try {
            return sessionFactory.getObject();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Bean
    public HibernateTransactionManager getTransactionManager() {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(getSessionFactory().getObject());
        return transactionManager;
    }

    Properties hibernateProperties() {
        return new Properties() {
            {
                setProperty("hibernate.dialect",
                        env.getProperty("hibernate.dialect"));
                setProperty("hibernate.show_sql",
                        env.getProperty("hibernate.show_sql"));
                setProperty("hibernate.format_sql",
                        env.getProperty("hibernate.format_sql"));
                setProperty("hibernate.connection.CharSet",
                        env.getProperty("hibernate.connection.CharSet"));
                setProperty("hibernate.connection.characterEncoding",
                        env.getProperty("hibernate.connection.characterEncoding"));
                setProperty("hibernate.connection.useUnicode",
                        env.getProperty("hibernate.connection.useUnicode"));
            }
        };
    }
}
