package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.sytoss.trainee.cinema.connector.dao.HallDao;
import org.sytoss.trainee.cinema.dto.HallDto;

import javax.transaction.Transactional;

@Repository
@Transactional
public class HallHibernateDao extends AbstractHibernateDao<HallDto> implements HallDao {

    @Override
    public HallDto findByName(String name) {
        Query resultQuery = super.currentSession().createQuery("FROM HallDto WHERE name ='" + name + "'");
        HallDto hall = (HallDto) resultQuery.getSingleResult();
        return hall;
    }

    @Override
    public void insert(HallDto hall) {
        super.insert(hall);
    }

    @Override
    public void update(HallDto hall) {
        super.update(hall);
    }

    @Override
    public HallDto findById(int id) {
        return super.findById(id);
    }
}
