package org.sytoss.trainee.cinema.connector;

import lombok.extern.slf4j.Slf4j;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.sytoss.trainee.cinema.exception.FileIsEmptyException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class FileSystemConnector {


    public void saveToFile(String path, byte[] content) throws IOException {
        log.trace("Start saving file");
        FileOutputStream fileOutputStream = new FileOutputStream(path);
        fileOutputStream.write(content);
        fileOutputStream.flush();
        fileOutputStream.close();
        log.trace("Successful saving");
    }

    public List<StringBuilder> loadCSV(File file) throws FileIsEmptyException, IOException {
        log.trace("Start loading CSV:" + file.getPath());
        List<StringBuilder> result = new ArrayList<>();
        InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(file), "utf-8");
        BufferedReader reader = new BufferedReader(inputStreamReader);
        for (String line; (line = reader.readLine()) != null; ) {
            result.add(new StringBuilder(line));
        }
        reader.close();
        if (result.isEmpty()) {
            log.warn("CSV file is empty!");
            throw new FileIsEmptyException("CSV file is empty!");
        }
        log.trace("Successful loading");
        return result;
    }

    public void saveCSV(List<StringBuilder> ticketTransferObjects, String pathToFile) throws IOException {
        log.trace("Start saving CSV");
        StringBuilder result = new StringBuilder();
        OutputStreamWriter fileWriter = new OutputStreamWriter(new FileOutputStream(pathToFile), StandardCharsets.UTF_8);
        for (StringBuilder ticketTransferObject : ticketTransferObjects) {
            result.append(ticketTransferObject.append(System.lineSeparator()));
        }
        fileWriter.append(result);
        fileWriter.flush();
        log.trace("Successful saving");
    }

    public Element loadXMLFile(String pathToFile) throws JDOMException, IOException {
        log.trace("Start load XML to Element");
        File fileForXML = new File(pathToFile);
        SAXBuilder saxBuilder = new SAXBuilder();
        Document documentForXML = saxBuilder.build(fileForXML);
        log.trace("Successful loading");
        return documentForXML.getRootElement();
    }

    public void saveToXmlWithJdom(Document document, String pathToFile) throws IOException {
        log.trace("Start saving XML with JDOM");
        XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        FileOutputStream fileOutputStream = new FileOutputStream(pathToFile);
        xmlOutputter.output(document, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
        log.trace("Successful saving");
    }

    public XmlPullParser loadXML(String path) throws IOException, XmlPullParserException {
        log.trace("Start load XML");
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser xpp = factory.newPullParser();
        InputStream input = new FileInputStream(path);
        xpp.setInput(new InputStreamReader(input));
        log.trace("Successful loading to XmlPullParser");
        return xpp;
    }
}