package org.sytoss.trainee.cinema.connector.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.sytoss.trainee.cinema.dto.CinemaDto;
import org.sytoss.trainee.cinema.dto.TicketWindowDto;

import java.util.List;

public interface TicketWindowDao extends AbstractDao<TicketWindowDto> {

    @Result(property = "cinemaDto", column = "id_cinema", javaType = CinemaDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.CinemaDao.findById"))
    @Select("SELECT * from ticket_window WHERE id_cinema = (SELECT id from cinema where name = #{cinema})")
    List<TicketWindowDto> findByCinema(String cinema);

    @Override
    @Insert("INSERT into ticket_window (id_cinema) VALUES(#{cinemaDto.id}")
    @SelectKey(statement = "call next value for seat_seq", keyProperty = "id", before = true, resultType = int.class)
    void insert(TicketWindowDto object);

    @Override
    @Update("UPDATE ticket_window SET id_cinema =#{cinemaDto.id}} WHERE id =#{id}")
    void update(TicketWindowDto object);

    @Override
    @Select("SELECT * from ticket_window WHERE id = #{id}")
    @Result(property = "cinemaDto", column = "id_cinema", javaType = CinemaDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.CinemaDao.findById"))
    TicketWindowDto findById(int id);
}
