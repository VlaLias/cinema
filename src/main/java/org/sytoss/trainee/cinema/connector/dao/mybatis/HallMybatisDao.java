package org.sytoss.trainee.cinema.connector.dao.mybatis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.sytoss.trainee.cinema.connector.dao.HallDao;
import org.sytoss.trainee.cinema.dto.HallDto;

import javax.transaction.Transactional;

@Repository
@Transactional
public class HallMybatisDao implements HallDao {

    @Autowired
    private HallDao hallDao;

    @Override
    public HallDto findByName(String name) {
        return hallDao.findByName(name);
    }

    @Override
    public void insert(HallDto object) {
        hallDao.insert(object);
    }

    @Override
    public void update(HallDto object) {
        hallDao.update(object);
    }

    @Override
    public HallDto findById(int id) {
        return hallDao.findById(id);
    }
}
