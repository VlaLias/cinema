package org.sytoss.trainee.cinema.connector.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.sytoss.trainee.cinema.dto.HallDto;
import org.sytoss.trainee.cinema.dto.SeatDto;

import java.util.List;

public interface SeatDao extends AbstractDao<SeatDto> {

    @Results({@Result(property = "hallDto", column = "id_hall", javaType = HallDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.HallDao.findById"))})
    @Select("SELECT * from seat WHERE id_hall = (select id from hall where name=#{name})")
    List<SeatDto> findByHall(String name);

    @Results({@Result(property = "hallDto", column = "id_hall", javaType = HallDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.HallDao.findById"))})
    @Select("SELECT * from seat WHERE line =#{line}")
    List<SeatDto> findByLine(int line);

    @Results({@Result(property = "hallDto", column = "id_hall", javaType = HallDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.HallDao.findById"))})
    @Select("SELECT * from seat WHERE place =#{place}")
    List<SeatDto> findByPlace(int place);

    @Override
    @Insert("INSERT into seat (place,line,id_hall) VALUES(#{place},#{line},#{hallDto.id})")
    @SelectKey(statement = "call next value for seat_seq", keyProperty = "id", before = true, resultType = int.class)
    void insert(SeatDto object);

    @Override
    @Update({"UPDATE seat SET place=#{place}, line =#{line}, id_hall = #{hallDto.id} WHERE id =#{id}"})
    void update(SeatDto object);

    @Override
    @Results({@Result(property = "hallDto", column = "id_hall", javaType = HallDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.HallDao.findById"))})
    @Select("SELECT * from seat WHERE id = #{id}")
    SeatDto findById(int id);
}
