package org.sytoss.trainee.cinema.connector.dao;

public interface AbstractDao<T> {

    void insert(T object);

    void update(T object);

    T findById(int id);
}
