package org.sytoss.trainee.cinema.connector.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.sytoss.trainee.cinema.dto.UserDto;


public interface UserDao extends AbstractDao<UserDto> {

    @Select("SELECT * from user WHERE Login = #{login}")
    UserDto findByLogin(String login);

    @Select("SELECT * from user WHERE Id = #{id}")
    UserDto findById(int id);

    @Insert("INSERT into user(login,password,discount) VALUES( #{login}, #{password}, #{discount})")
    @SelectKey(statement = "call next value for user_seq", keyProperty = "id", before = true, resultType = int.class)
    void insert(UserDto user);

    @Update("UPDATE user SET Login=#{login}, Password=#{password}, Discount =#{discount} WHERE id =#{id}")
    void update(UserDto user);
}
