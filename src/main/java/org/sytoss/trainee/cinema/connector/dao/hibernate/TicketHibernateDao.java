package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.sytoss.trainee.cinema.connector.dao.TicketDao;
import org.sytoss.trainee.cinema.dto.TicketDto;
import org.sytoss.trainee.cinema.dto.UserDto;

import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class TicketHibernateDao extends AbstractHibernateDao<TicketDto> implements TicketDao {

    @Override
    public List<TicketDto> findBySeance(int id) {
        Session session = super.currentSession();
        Query query = session.createQuery("from TicketDto where id_seance =" + id);
        List<TicketDto> result = query.getResultList();
        return result;
    }

    @Override
    public List<TicketDto> findByUser(String login) {
        Session session = super.currentSession();
        UserDto resultUser = (UserDto) session.createQuery("from UserDto where Login = '" + login + "'").getSingleResult();
        Query query = session.createQuery("from TicketDto where id_user = " + resultUser.getId());
        List<TicketDto> result = query.getResultList();
        return result;
    }

    @Override
    public void insert(TicketDto ticket) {
        super.insert(ticket);
    }

    @Override
    public void update(TicketDto ticket) {
        super.update(ticket);
    }

    @Override
    public TicketDto findById(int id) {
        return super.findById(id);
    }
}
