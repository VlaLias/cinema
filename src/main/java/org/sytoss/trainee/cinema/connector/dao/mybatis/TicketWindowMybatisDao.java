package org.sytoss.trainee.cinema.connector.dao.mybatis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.sytoss.trainee.cinema.connector.dao.TicketWindowDao;
import org.sytoss.trainee.cinema.dto.TicketWindowDto;

import java.util.List;

@Repository
@Transactional
public class TicketWindowMybatisDao implements TicketWindowDao {

    @Autowired
    private TicketWindowDao ticketWindowDao;

    @Override
    public List<TicketWindowDto> findByCinema(String cinema) {
        return ticketWindowDao.findByCinema(cinema);
    }

    @Override
    public TicketWindowDto findById(int id) {
        return ticketWindowDao.findById(id);
    }

    @Override
    public void insert(TicketWindowDto ticketWindowDto) {
        ticketWindowDao.insert(ticketWindowDto);
    }

    @Override
    public void update(TicketWindowDto ticketWindowDto) {
        ticketWindowDao.update(ticketWindowDto);
    }
}
