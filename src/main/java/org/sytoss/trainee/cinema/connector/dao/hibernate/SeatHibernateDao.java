package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.sytoss.trainee.cinema.connector.dao.SeatDao;
import org.sytoss.trainee.cinema.dto.HallDto;
import org.sytoss.trainee.cinema.dto.SeatDto;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class SeatHibernateDao extends AbstractHibernateDao<SeatDto> implements SeatDao {

    @Override
    public List<SeatDto> findByHall(String hallName) {
        HallDto hall = (HallDto) super.currentSession().createQuery("FROM HallDto WHERE name ='" + hallName + "'").getSingleResult();
        Query resultQuery = super.currentSession().createQuery("FROM SeatDto WHERE id_hall =" + hall.getId());
        List<SeatDto> seats = resultQuery.list();
        return seats;
    }

    @Override
    public List<SeatDto> findByLine(int line) {
        Query resultQuery = super.currentSession().createQuery("FROM SeatDto WHERE Line =" + line);
        List<SeatDto> seats = resultQuery.list();
        return seats;
    }

    @Override
    public List<SeatDto> findByPlace(int place) {
        Query resultQuery = super.currentSession().createQuery("FROM SeatDto WHERE Place =" + place);
        List<SeatDto> seats = resultQuery.list();
        return seats;
    }

    @Override
    public void insert(SeatDto seat) {
        super.insert(seat);
    }

    @Override
    public void update(SeatDto seat) {
        super.update(seat);
    }

    @Override
    public SeatDto findById(int id) {
        return super.findById(id);
    }
}
