package org.sytoss.trainee.cinema.connector.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.sytoss.trainee.cinema.dto.CinemaDto;
import org.sytoss.trainee.cinema.dto.HallDto;

public interface HallDao extends AbstractDao<HallDto> {

    @Results({@Result(property = "cinemaDto", column = "id_cinema", javaType = CinemaDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.CinemaDao.findById"))})
    @Select("SELECT * from hall WHERE name = #{name}")
    HallDto findByName(String name);

    @Override
    @Insert("INSERT into hall (name,id_cinema) VALUES(#{name},#{id_cinema})")
    @SelectKey(statement = "call next value for hall_seq", keyProperty = "id", before = true, resultType = int.class)
    void insert(HallDto object);

    @Override
    @Update("UPDATE hall SET name=#{name}, id_cinema =#{id_cinema} WHERE id =#{id}")
    void update(HallDto object);

    @Override
    @Results({@Result(property = "cinemaDto", column = "id_cinema", javaType = CinemaDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.CinemaDao.findById"))})
    @Select("SELECT * from hall WHERE id = #{id}")
    HallDto findById(int id);
}
