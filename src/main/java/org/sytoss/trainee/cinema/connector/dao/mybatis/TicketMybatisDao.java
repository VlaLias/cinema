package org.sytoss.trainee.cinema.connector.dao.mybatis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.sytoss.trainee.cinema.connector.dao.TicketDao;
import org.sytoss.trainee.cinema.dto.TicketDto;

import java.util.List;

@Repository
@Transactional
public class TicketMybatisDao implements TicketDao {

    @Autowired
    private TicketDao ticketDao;

    @Override
    public List<TicketDto> findBySeance(int id) {
        return ticketDao.findBySeance(id);
    }

    @Override
    public List<TicketDto> findByUser(String user) {
        return ticketDao.findByUser(user);
    }

    @Override
    public TicketDto findById(int id) {
        return ticketDao.findById(id);
    }

    @Override
    public void insert(TicketDto ticketDto) {
        ticketDao.insert(ticketDto);
    }

    @Override
    public void update(TicketDto ticketDto) {
        ticketDao.update(ticketDto);
    }
}
