package org.sytoss.trainee.cinema.connector.dao.mybatis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.sytoss.trainee.cinema.connector.dao.SeanceDao;
import org.sytoss.trainee.cinema.dto.SeanceDto;

import java.sql.Date;
import java.util.List;

@Slf4j
@Repository
@Transactional
public class SeanceMybatisDao implements SeanceDao {

    @Autowired
    private SeanceDao seanceDao;

    @Override
    public List<SeanceDto> findByDate(Date date) {
        log.warn(seanceDao.findByDate(date).toString());
        return seanceDao.findByDate(date);
    }

    @Override
    public List<SeanceDto> findByHall(String hall) {
        return seanceDao.findByHall(hall);
    }

    @Override
    public List<SeanceDto> findByFilm(String film) {
        return seanceDao.findByFilm(film);
    }

    @Override
    public List<SeanceDto> findByCinema(String cinema) {
        return seanceDao.findByCinema(cinema);
    }

    @Override
    public SeanceDto findById(int id) {
        return seanceDao.findById(id);
    }

    @Override
    public void insert(SeanceDto seanceDto) {
        seanceDao.insert(seanceDto);
    }

    @Override
    public void update(SeanceDto seanceDto) {
        seanceDao.update(seanceDto);
    }
}
