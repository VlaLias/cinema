package org.sytoss.trainee.cinema.connector.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.sytoss.trainee.cinema.dto.CinemaDto;
import org.sytoss.trainee.cinema.dto.FilmDto;
import org.sytoss.trainee.cinema.dto.HallDto;
import org.sytoss.trainee.cinema.dto.SeanceDto;

import java.sql.Date;
import java.util.List;

public interface SeanceDao extends AbstractDao<SeanceDto> {

    @Results({
            @Result(property = "hallDto", column = "id_hall", javaType = HallDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.HallDao.findById")),
            @Result(property = "cinemaDto", column = "id_cinema", javaType = CinemaDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.CinemaDao.findById")),
            @Result(property = "filmDto", column = "id_film", javaType = FilmDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.FilmDao.findById"))
    })
    @Select("SELECT * from seance WHERE (DAY(start_date) = DAY(#{date})) AND (MONTH(start_date) = MONTH(#{date})) AND (YEAR(start_date) = YEAR(#{date}))")
    List<SeanceDto> findByDate(Date date);

    @Results({
            @Result(property = "hallDto", column = "id_hall", javaType = HallDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.HallDao.findById")),
            @Result(property = "cinemaDto", column = "id_cinema", javaType = CinemaDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.CinemaDao.findById")),
            @Result(property = "filmDto", column = "id_film", javaType = FilmDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.FilmDao.findById"))
    })
    @Select("SELECT * from seance WHERE id_hall = (SELECT id from hall where name = #{hall})")
    List<SeanceDto> findByHall(String hall);

    @Results({
            @Result(property = "hallDto", column = "id_hall", javaType = HallDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.HallDao.findById")),
            @Result(property = "cinemaDto", column = "id_cinema", javaType = CinemaDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.CinemaDao.findById")),
            @Result(property = "filmDto", column = "id_film", javaType = FilmDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.FilmDao.findById"))
    })
    @Select("SELECT * from seance WHERE id_film = (SELECT id from film where name = #{film})")
    List<SeanceDto> findByFilm(String film);

    @Results({
            @Result(property = "hallDto", column = "id_hall", javaType = HallDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.HallDao.findById")),
            @Result(property = "cinemaDto", column = "id_cinema", javaType = CinemaDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.CinemaDao.findById")),
            @Result(property = "filmDto", column = "id_film", javaType = FilmDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.FilmDao.findById"))
    })
    @Select("SELECT * from seance WHERE id_cinema = (SELECT id from cinema where name = #{cinema})")
    List<SeanceDto> findByCinema(String cinema);

    @Override
    @Insert("INSERT into seance (default_price,start_date,id_hall,id_film,id_cinema}) VALUES(#{default_price}," +
            "#{start_date},#{hallDto.id}),#{filmDto.id},#{cinemaDto.id})")
    @SelectKey(statement = "call next value for seat_seq", keyProperty = "id", before = true, resultType = int.class)
    void insert(SeanceDto object);

    @Override
    @Update("UPDATE seance SET default_price=#{default_price}, start_date =#{start_date}," +
            "id_hall=#{hallDto.id}, id_film =#{filmDto.id}, id_cinema =#{cinemaDto.id}} WHERE id =#{id}")
    void update(SeanceDto object);

    @Override
    @Results({
            @Result(property = "hallDto", column = "id_hall", javaType = HallDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.HallDao.findById")),
            @Result(property = "cinemaDto", column = "id_cinema", javaType = CinemaDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.CinemaDao.findById")),
            @Result(property = "filmDto", column = "id_film", javaType = FilmDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.FilmDao.findById"))
    })
    @Select("SELECT * from seance WHERE id = #{id}")
    SeanceDto findById(int id);
}
