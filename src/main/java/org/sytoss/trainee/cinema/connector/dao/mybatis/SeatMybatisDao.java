package org.sytoss.trainee.cinema.connector.dao.mybatis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.sytoss.trainee.cinema.connector.dao.SeatDao;
import org.sytoss.trainee.cinema.dto.SeatDto;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class SeatMybatisDao implements SeatDao {

    @Autowired
    private SeatDao seatDao;

    @Override
    public List<SeatDto> findByHall(String name) {
        return seatDao.findByHall(name);
    }

    @Override
    public List<SeatDto> findByLine(int line) {
        return seatDao.findByLine(line);
    }

    @Override
    public List<SeatDto> findByPlace(int place) {
        return seatDao.findByPlace(place);
    }

    @Override
    public void insert(SeatDto object) {
        seatDao.insert(object);
    }

    @Override
    public void update(SeatDto object) {
        seatDao.update(object);
    }

    @Override
    public SeatDto findById(int id) {
        return seatDao.findById(id);
    }
}
