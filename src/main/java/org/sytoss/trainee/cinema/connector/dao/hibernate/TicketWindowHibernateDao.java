package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.sytoss.trainee.cinema.connector.dao.TicketWindowDao;
import org.sytoss.trainee.cinema.dto.CinemaDto;
import org.sytoss.trainee.cinema.dto.TicketWindowDto;

import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class TicketWindowHibernateDao extends AbstractHibernateDao<TicketWindowDto> implements TicketWindowDao {

    @Override
    public List<TicketWindowDto> findByCinema(String name) {
        Session session = super.currentSession();
        CinemaDto resultCinema = (CinemaDto) session.createQuery("from CinemaDto where name ='" + name + "'").getSingleResult();
        Query query = session.createQuery("from TicketWindowDto where id_cinema = " + resultCinema.getId());
        List<TicketWindowDto> result = query.getResultList();
        return result;
    }

    @Override
    public void insert(TicketWindowDto ticketWindow) {
        super.insert(ticketWindow);
    }

    @Override
    public void update(TicketWindowDto ticketWindow) {
        super.update(ticketWindow);
    }

    @Override
    public TicketWindowDto findById(int id) {
        return super.findById(id);
    }
}
