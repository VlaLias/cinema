package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.sytoss.trainee.cinema.connector.dao.SeanceDao;
import org.sytoss.trainee.cinema.dto.CinemaDto;
import org.sytoss.trainee.cinema.dto.FilmDto;
import org.sytoss.trainee.cinema.dto.HallDto;
import org.sytoss.trainee.cinema.dto.SeanceDto;

import javax.persistence.Query;
import javax.transaction.Transactional;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

@Repository
@Transactional
public class SeanceHibernateDao extends AbstractHibernateDao<SeanceDto> implements SeanceDao {

    @Override
    public List<SeanceDto> findByDate(Date date) {
        Session session = super.currentSession();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, 1);
        Date nextDay = new Date(calendar.getTimeInMillis());
        List<SeanceDto> result = session.createQuery("from SeanceDto where Start_Date between '" + date
                + "' and '" + nextDay + "'").getResultList();
        return result;
    }

    @Override
    public List<SeanceDto> findByHall(String hall) {
        Session session = super.currentSession();
        HallDto resultHall = (HallDto) session.createQuery("from  HallDto where name ='" + hall + "'").getSingleResult();
        Query query = session.createQuery("from SeanceDto where id_hall =" + resultHall.getId());
        List<SeanceDto> result = query.getResultList();
        return result;
    }

    @Override
    public List<SeanceDto> findByFilm(String film) {
        Session session = super.currentSession();
        FilmDto resultFilm = (FilmDto) session.createQuery("from FilmDto where name = '" + film + "'").getSingleResult();
        Query query = session.createQuery("from SeanceDto where id_film = " + resultFilm.getId());
        List<SeanceDto> result = query.getResultList();
        return result;
    }

    @Override
    public List<SeanceDto> findByCinema(String cinema) {
        Session session = super.currentSession();
        CinemaDto resultCinema = (CinemaDto) session.createQuery("from CinemaDto where name ='" + cinema + "'").getSingleResult();
        Query query = session.createQuery("from SeanceDto where id_cinema = " + resultCinema.getId());
        List<SeanceDto> result = query.getResultList();
        return result;
    }

    @Override
    public void insert(SeanceDto seance) {
        super.insert(seance);
    }

    @Override
    public void update(SeanceDto seance) {
        super.update(seance);
    }

    @Override
    public SeanceDto findById(int id) {
        return super.findById(id);
    }
}
