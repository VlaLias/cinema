package org.sytoss.trainee.cinema.connector.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.sytoss.trainee.cinema.dto.CinemaDto;

public interface CinemaDao extends AbstractDao<CinemaDto> {

    @Select("SELECT * from cinema WHERE name = #{name}")
    CinemaDto findByName(String name);

    @Override
    @Insert("INSERT into cinema (name,address) VALUES(#{name},#{address})")
    @SelectKey(statement = "call next value for cinema_seq", keyProperty = "id", before = true, resultType = int.class)
    void insert(CinemaDto object);

    @Override
    @Update("UPDATE cinema SET name=#{name}, address =#{address} WHERE id =#{id}")
    void update(CinemaDto object);

    @Override
    @Select("SELECT * from cinema WHERE id = #{id}")
    CinemaDto findById(int id);
}
