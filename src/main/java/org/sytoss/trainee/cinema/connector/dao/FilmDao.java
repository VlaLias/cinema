package org.sytoss.trainee.cinema.connector.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.sytoss.trainee.cinema.dto.FilmDto;

public interface FilmDao extends AbstractDao<FilmDto> {

    @Select("SELECT * from film WHERE name = #{name}")
    FilmDto findByName(String name);

    @Override
    @Insert("INSERT into film (name,duration) VALUES(#{name},#{duration})")
    @SelectKey(statement = "call next value for film_seq", keyProperty = "id", before = true, resultType = int.class)
    void insert(FilmDto object);

    @Override
    @Update("UPDATE film SET name=#{name}, duration =#{duration} WHERE id =#{id}")
    void update(FilmDto object);

    @Override
    @Select("SELECT * from film WHERE id = #{id}")
    FilmDto findById(int id);
}
