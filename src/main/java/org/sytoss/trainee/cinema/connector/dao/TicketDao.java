package org.sytoss.trainee.cinema.connector.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.sytoss.trainee.cinema.dto.SeanceDto;
import org.sytoss.trainee.cinema.dto.SeatDto;
import org.sytoss.trainee.cinema.dto.TicketDto;
import org.sytoss.trainee.cinema.dto.TicketWindowDto;
import org.sytoss.trainee.cinema.dto.UserDto;

import java.util.List;

public interface TicketDao extends AbstractDao<TicketDto> {

    @Select("SELECT * from ticket WHERE id_seance = #{seance}")
    List<TicketDto> findBySeance(int seance);

    @Select("SELECT * from ticket WHERE id_user = (SELECT id from user where login = #{user})")
    List<TicketDto> findByUser(String user);

    @Override
    @Insert("INSERT into ticket (price,id_ticketwindow,id_seance,id_user,id_seat}) VALUES(#{price}," +
            "#{ticketWindowDto.id},#{seanceDto.id},#{userDto.id},#{seatDto.id})")
    @SelectKey(statement = "call next value for seat_seq", keyProperty = "id", before = true, resultType = int.class)
    void insert(TicketDto object);

    @Override
    @Update("UPDATE ticket SET price=#{price}, id_ticketwindow =#{ticketWindowDto.id}, id_seance =#{seanceDto.id}," +
            ", id_user =#{userDto.id}, id_seat =#{seatDto.id}} WHERE id =#{id}")
    void update(TicketDto object);

    @Override
    @Select("SELECT * from ticket WHERE id = #{id}")
    @Results({
            @Result(property = "seanceDto", column = "id_seance", javaType = SeanceDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.SeanceDao.findById")),
            @Result(property = "ticketWindowDto", column = "id_ticketwindow", javaType = TicketWindowDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.TicketWindowDao.findById")),
            @Result(property = "userDto", column = "id_user", javaType = UserDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.UserDao.findById")),
            @Result(property = "seatDto", column = "id_seat", javaType = SeatDto.class, one = @One(select = "org.sytoss.trainee.cinema.connector.dao.SeatDao.findById"))
    })
    TicketDto findById(int id);

}
