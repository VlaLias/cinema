package org.sytoss.trainee.cinema.connector.dao.mybatis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.sytoss.trainee.cinema.connector.dao.FilmDao;
import org.sytoss.trainee.cinema.dto.FilmDto;

import javax.transaction.Transactional;

@Repository
@Transactional
public class FilmMybatisDao implements FilmDao {

    @Autowired
    private FilmDao filmDao;

    @Override
    public FilmDto findByName(String name) {
        return filmDao.findByName(name);
    }

    @Override
    public void insert(FilmDto object) {
        filmDao.insert(object);
    }

    @Override
    public void update(FilmDto object) {
        filmDao.update(object);
    }

    @Override
    public FilmDto findById(int id) {
        return filmDao.findById(id);
    }
}
