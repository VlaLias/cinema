package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.sytoss.trainee.cinema.connector.dao.CinemaDao;
import org.sytoss.trainee.cinema.dto.CinemaDto;

import javax.transaction.Transactional;

@Repository
@Transactional
public class CinemaHibernateDao extends AbstractHibernateDao<CinemaDto> implements CinemaDao {

    @Override
    public CinemaDto findByName(String name) {
        Query resultQuery = super.currentSession().createQuery("FROM CinemaDto WHERE name ='" + name + "'");
        return (CinemaDto) resultQuery.getSingleResult();
    }

    @Override
    public void insert(CinemaDto cinema) {
        super.insert(cinema);
    }

    @Override
    public void update(CinemaDto cinema) {
        super.update(cinema);
    }

    @Override
    public CinemaDto findById(int id) {
        return super.findById(id);
    }
}
