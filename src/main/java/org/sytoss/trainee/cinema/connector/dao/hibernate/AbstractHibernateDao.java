package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.sytoss.trainee.cinema.connector.dao.AbstractDao;

import javax.transaction.Transactional;
import java.lang.reflect.ParameterizedType;

@Transactional
public abstract class AbstractHibernateDao<T> implements AbstractDao<T> {

    @Autowired
    private SessionFactory sessionFactory;

    private Class<T> entityClass;

    public AbstractHibernateDao() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
    }

    protected Session currentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public void insert(final T object) {
        currentSession().save(object);
    }

    @Override
    public void update(final T object) {
        currentSession().update(object);
    }

    @Override
    public T findById(int id) {
        return (T) currentSession().get(entityClass, id);
    }
}