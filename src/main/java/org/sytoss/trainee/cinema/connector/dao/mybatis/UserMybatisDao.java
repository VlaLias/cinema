package org.sytoss.trainee.cinema.connector.dao.mybatis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.sytoss.trainee.cinema.connector.dao.UserDao;
import org.sytoss.trainee.cinema.dto.UserDto;

@Repository
@Transactional
public class UserMybatisDao implements UserDao {

    @Autowired
    private UserDao userDao;

    @Override
    public UserDto findByLogin(String login) {
        return userDao.findByLogin(login);
    }

    @Override
    public UserDto findById(int id) {
        return userDao.findById(id);
    }

    @Override
    public void insert(UserDto user) {
        userDao.insert(user);
    }

    @Override
    public void update(UserDto user) {
        userDao.update(user);
    }
}
