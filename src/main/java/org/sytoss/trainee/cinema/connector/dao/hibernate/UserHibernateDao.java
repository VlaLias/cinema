package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.sytoss.trainee.cinema.connector.dao.UserDao;
import org.sytoss.trainee.cinema.dto.UserDto;

import javax.transaction.Transactional;

@Repository
@Transactional
public class UserHibernateDao extends AbstractHibernateDao<UserDto> implements UserDao {

    @Override
    public UserDto findByLogin(String login) {
        Session session = super.currentSession();
        UserDto result = (UserDto) session.createQuery("from UserDto where Login ='" + login + "'").getSingleResult();
        return result;
    }

    @Override
    public void insert(UserDto registerUser) {
        super.insert(registerUser);
    }

    @Override
    public void update(UserDto registerUser) {
        super.update(registerUser);
    }

    @Override
    public UserDto findById(int id) {
        return super.findById(id);
    }

}
