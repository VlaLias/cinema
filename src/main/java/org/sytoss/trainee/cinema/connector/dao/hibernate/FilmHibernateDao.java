package org.sytoss.trainee.cinema.connector.dao.hibernate;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.sytoss.trainee.cinema.connector.dao.FilmDao;
import org.sytoss.trainee.cinema.dto.FilmDto;

import javax.transaction.Transactional;

@Repository
@Transactional
public class FilmHibernateDao extends AbstractHibernateDao<FilmDto> implements FilmDao {

    @Override
    public FilmDto findByName(String name) {
        Query resultQuery = super.currentSession().createQuery("FROM FilmDto WHERE name ='" + name + "'");
        return (FilmDto) resultQuery.getSingleResult();
    }

    @Override
    public void insert(FilmDto film) {
        super.insert(film);
    }

    @Override
    public void update(FilmDto film) {
        super.update(film);
    }

    @Override
    public FilmDto findById(int id) {
        return super.findById(id);
    }
}
