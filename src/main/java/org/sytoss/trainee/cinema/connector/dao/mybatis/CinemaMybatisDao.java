package org.sytoss.trainee.cinema.connector.dao.mybatis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.sytoss.trainee.cinema.connector.dao.CinemaDao;
import org.sytoss.trainee.cinema.dto.CinemaDto;

import javax.transaction.Transactional;

@Repository
@Transactional
public class CinemaMybatisDao implements CinemaDao {

    @Autowired
    private CinemaDao cinemaDao;

    @Override
    public CinemaDto findByName(String name) {
        return cinemaDao.findByName(name);
    }

    @Override
    public void insert(CinemaDto object) {
        cinemaDao.insert(object);
    }

    @Override
    public void update(CinemaDto object) {
        cinemaDao.update(object);
    }

    @Override
    public CinemaDto findById(int id) {
        return cinemaDao.findById(id);
    }
}
