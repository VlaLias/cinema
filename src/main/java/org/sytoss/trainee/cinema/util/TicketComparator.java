package org.sytoss.trainee.cinema.util;

import org.sytoss.trainee.cinema.dom.Ticket;

import java.util.Comparator;

public class TicketComparator implements Comparator<Ticket> {

    @Override
    public int compare(Ticket t1, Ticket t2) {
        int result = t1.getTicketWindow().compareTo(t2.getTicketWindow());
        if (result != 0) {
            return result;
        }

        result = t1.getSeance().getFilm().getName().compareTo(t2.getSeance().getFilm().getName());
        if (result != 0) {
            return result;
        }

        result = t1.getSeance().getHall().getName().compareTo(t2.getSeance().getHall().getName());
        if (result != 0) {
            return result;
        }

        result = t1.getSeance().getStartDate().compareTo(t2.getSeance().getStartDate());
        if (result != 0) {
            return result;
        }

        result = t1.getId() - t2.getId();
        if (result != 0) {
            return result;
        }
        return 0;
    }
}
