package org.sytoss.trainee.cinema.util;

import org.jdom2.Element;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
@PropertySource("classpath:constant.properties")
public final class TranslatorUtil {

    @Value("${SYMBOLSPATTERN}")
    private String symbolsPattern;

    @Value("${DATEPATTERN}")
    private String datePattern;

    public static String parsePhrase(String phrase, String quote) {
        phrase = phrase.replace("\"", "" + quote + quote);
        return phrase;
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    public static Element findByAttrValue(Element sourceElement, String attribute, String value) {
        List<Element> result;
        try {
            result = sourceElement.getChildren().stream()
                    .filter(p -> p.getAttribute(attribute).getValue().equals(value))
                    .collect(Collectors.toList());
        } catch (NullPointerException e) {
            result = null;
        }
        if (result != null && result.size() > 0) {
            return result.get(0);
        }
        return null;
    }

    public static int parseStringToInt(String string) {
        int number;
        try {
            number = Integer.parseInt(string);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("String does not contain a parsable integer.");
        }
        return number;
    }

    public static double parseStringToDouble(String string) {
        double number;
        try {
            number = Double.parseDouble(string);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("String does not contain a parsable integer.");
        }
        return number;
    }

    public StringBuilder wrapWithQuotes(String phrase, String quote) {
        if (!checkForSymbols(phrase)) {
            return new StringBuilder().append(quote + phrase + quote);
        } else {
            return new StringBuilder(phrase);
        }
    }

    public boolean checkForSymbols(String phrase) {
        Pattern pattern = Pattern.compile(symbolsPattern);
        Matcher m = pattern.matcher(phrase);
        return m.matches();
    }

    public DateTime parseStringToDate(String string) {
        DateTimeFormatter dtf = DateTimeFormat.forPattern(datePattern);
        DateTime date = dtf.parseDateTime(string);
        return date;
    }
}
