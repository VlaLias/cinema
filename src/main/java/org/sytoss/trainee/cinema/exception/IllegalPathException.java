package org.sytoss.trainee.cinema.exception;

public class IllegalPathException extends RuntimeException {

    public IllegalPathException() {
        super();
    }

    public IllegalPathException(String message) {
        super(message);
    }
}