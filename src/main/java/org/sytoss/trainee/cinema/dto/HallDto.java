package org.sytoss.trainee.cinema.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@ToString
@Table(name = "Hall")
@Getter
@Setter
public class HallDto {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hall_id_seq")
    @SequenceGenerator(name = "hall_id_seq", sequenceName = "hall_seq", allocationSize = 1)
    @Column(name = "id")
    @Setter(AccessLevel.NONE)
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cinema", nullable = false)
    private CinemaDto cinemaDto;

}
