package org.sytoss.trainee.cinema.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "Seat")
@ToString
@Getter
@Setter
public class SeatDto {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seat_id_seq")
    @SequenceGenerator(name = "seat_id_seq", sequenceName = "seat_seq", allocationSize = 1)
    @Column(name = "id")
    @Setter(AccessLevel.NONE)
    private int id;

    @Column(name = "Place")
    private int place;

    @Column(name = "Line")
    private int line;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_hall", nullable = false)
    private HallDto hallDto;

}
