package org.sytoss.trainee.cinema.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.sql.Date;


@Entity
@ToString
@Table(name = "Seance")
@Getter
@Setter
public class SeanceDto {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seance_id_seq")
    @SequenceGenerator(name = "seance_id_seq", sequenceName = "seance_seq", allocationSize = 1)
    @Column(name = "id")
    @Setter(AccessLevel.NONE)
    private int id;

    @Column(name = "Default_Price")
    private Double price;

    @Column(name = "Start_Date")
    private Date startDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_hall", nullable = false)
    private HallDto hallDto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_film", nullable = false)
    private FilmDto filmDto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cinema", nullable = false)
    private CinemaDto cinemaDto;
}
